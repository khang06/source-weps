SWEP.Base = "weapon_hl2base"
SWEP.Category = "Counter-Strike: Global Offensive"
SWEP.IsSWCSWeapon = true

AccessorFunc(SWEP, "m_iUnsharedSeed", "UnsharedSeed", FORCE_NUMBER)

local IronSight_should_approach_unsighted = 0
local IronSight_should_approach_sighted = 1
local IronSight_viewmodel_is_deploying = 2
local IronSight_weapon_is_dropped = 3

local bIsTTT = util.NetworkStringToID("TTT_RoundState") ~= 0

if bIsTTT then
    SWEP.bIsTTT = true
    --SWEP.AutoSpawnable = false
end

SURFACE_PROP_DEFAULT = util.GetSurfaceIndex("default")

CS_MASK_SHOOT = bit.bor(MASK_SHOT, CONTENTS_DEBRIS)

Primary_Mode = 0
Secondary_Mode = 1

DEFINE_BASECLASS(SWEP.Base)

sound.Add({
    name = "Default.NearlyEmpty",
    channel = CHAN_ITEM,
    level = 65,
    volume = 1,
    sound = "weapons/csgo/lowammo_01.wav"
})
sound.Add({
    name = "Weapon.WeaponMove1",
    channel = CHAN_ITEM,
    level = 65,
    volume = {0.05, 0.1},
    pitch = {98, 101},
    sound = "weapons/csgo/movement1.wav"
})
sound.Add({
    name = "Weapon.WeaponMove2",
    channel = CHAN_ITEM,
    level = 65,
    volume = {0.05, 0.1},
    pitch = {98, 101},
    sound = "weapons/csgo/movement2.wav"
})
sound.Add({
    name = "Weapon.WeaponMove3",
    channel = CHAN_ITEM,
    level = 65,
    volume = {0.05, 0.1},
    pitch = {98, 101},
    sound = "weapons/csgo/movement3.wav"
})
sound.Add({
    name = "Weapon.AutoSemiAutoSwitch",
    channel = CHAN_STATIC,
    level = 65,
    volume = 1.0,
    pitch = {98, 101},
    sound = "weapons/csgo/auto_semiauto_switch.wav"
})

AccessorFunc(SWEP, "m_sWeaponType", "WeaponType", FORCE_STRING)
AccessorFunc(SWEP, "m_sZoomOutSound", "ZoomOutSound", FORCE_STRING)
AccessorFunc(SWEP, "m_sZoomInSound", "ZoomInSound", FORCE_STRING)
AccessorFunc(SWEP, "m_flDeploySpeed", "DeploySpeed", FORCE_NUMBER)

SWEP.UseHands = true
SWEP.ViewModelFOV = 68

local weapon_air_spread_scale = GetConVar"weapon_air_spread_scale"
local weapon_recoil_decay_coefficient = GetConVar"weapon_recoil_decay_coefficient"
local weapon_accuracy_nospread = GetConVar"weapon_accuracy_nospread"

local sv_showimpacts = GetConVar("sv_showimpacts")
local sv_showimpacts_penetration = GetConVar("sv_showimpacts_penetration")
local sv_showimpacts_time = GetConVar("sv_showimpacts_time")

function SWEP:GetShotgunReloadState() return 0 end -- only shotguns use this for multi-stage reloads

function SWEP:SetupDataTables()
    BaseClass.SetupDataTables(self)
    self:NetworkVar("Bool", 2, "InInspect")
    self:NetworkVar("Bool", 3, "SilencerOn")
    self:NetworkVar("Bool", 4, "ResumeZoom")
    self:NetworkVar("Bool", 5, "BurstMode")
    self:NetworkVar("Bool", 6, "IsLookingAtWeapon")

    self:NetworkVar("Int", 1, "ZoomLevel")
    self:NetworkVar("Int", 2, "WeaponMode")
    self:NetworkVar("Int", 3, "BurstShotsRemaining")
    self:NetworkVar("Int", 4, "SharedSeed")
    self:NetworkVar("Int", 5, "IronSightMode")

    self:NetworkVar("Float", 3, "AccuracyPenalty")
    self:NetworkVar("Float", 4, "RecoilIndex")
    self:NetworkVar("Float", 5, "AccuracyFishtail")
    self:NetworkVar("Float", 6, "DoneSwitchingSilencer")
    self:NetworkVar("Float", 7, "NextBurstShot")
    self:NetworkVar("Float", 8, "PostponeFireReadyTime")
    self:NetworkVar("Float", 9, "LookWeaponEndTime")
    self:NetworkVar("Float", 10,"WeaponIdleTime")

    self:NetworkVar("Angle", 0, "RawAimPunchAngle")
    self:NetworkVar("Angle", 1, "AimPunchAngleVel")
    self:NetworkVar("Angle", 2, "ViewPunchAngle")
end

function SWEP:GetRandomSeed()
    if SWCS_SPREAD_SHARE_SEED:GetBool() then
        return bit.band(math.floor(self:GetSharedSeed()), 255)
    else
        return bit.band(math.floor(self:GetUnsharedSeed() or 0), 255)
    end
end
function SWEP:SetRandomSeed(iSeed)
    if SWCS_SPREAD_SHARE_SEED:GetBool() then
        self:SetSharedSeed(math.floor(iSeed))
    else
        self:SetUnsharedSeed(math.floor(iSeed))
    end
end

local function MACRO__SetupItemDefGetter(tab, name, attribute, force_type, scale, default)
    local fnName = "Get" .. name
    scale = scale or 1
    default = default or 0

    if force_type == FORCE_BOOL then
        tab[fnName] = function(self)
            return tobool(self.ItemAttributes[attribute] or default)
        end
    elseif force_type == FORCE_STRING then
        tab[fnName] = function(self)
            return tostring(self.ItemAttributes[attribute])
        end
    else -- assume number
        tab[fnName] = function(self)
            return (self.ItemAttributes[attribute] or default) * scale
        end
    end
end
local function MACRO__SetupItemDefGetterHasAlt(tab, name, attribute, force_type, scale, default)
    local fnName = "Get" .. name

    MACRO__SetupItemDefGetter(tab, name .. "1", attribute, force_type, scale, default)
    MACRO__SetupItemDefGetter(tab, name .. "2", attribute .. " alt", force_type, scale, default)

    tab[fnName] = function(self)
        if self:GetWeaponMode() == Primary_Mode then
            return self["Get" .. name .. "1"](self)
        else
            return self["Get" .. name .. "2"](self)
        end
    end
end

local weapon_recoil_scale = GetConVar"weapon_recoil_scale"
function SWEP:GetAimPunchAngle()
    return self:GetRawAimPunchAngle() * weapon_recoil_scale:GetFloat()
end

local speed = GetConVar"sv_defaultdeployspeed"
function SWEP:GetDeploySpeed()
    return self.bIsTTT and 1.4 or speed:GetFloat()
end

SWEP.ItemDefAttributes = [=["attributes 04/22/2020" {}]=]
SWEP.ItemDefVisuals = [=["visuals 07/07/2020" {}]=]
SWEP.ItemDefPrefab = [=["prefab 08/11/2020" {}]=]
function SWEP:Initialize()
    local attributes = util.KeyValuesToTable(self.ItemDefAttributes, true, false)
    self.ItemAttributes = attributes
    local visuals = util.KeyValuesToTable(self.ItemDefVisuals, true, false)
    self.ItemVisuals = visuals
    local prefab = util.KeyValuesToTable(self.ItemDefPrefab, true, false)
    self.ItemPrefab = prefab

    -- csgo doesn't share the random seed, so we try to emulate that here
    -- servers and clients will never have the exact same uptime
    self:SetUnsharedSeed( SysTime() / engine.TickInterval() )
    self:SetSharedSeed(0)

    MACRO__SetupItemDefGetter(self, "DefCycleTime", "cycletime")
    MACRO__SetupItemDefGetter(self, "MaxSpeed", "max player speed")
    MACRO__SetupItemDefGetter(self, "Damage", "damage")
    MACRO__SetupItemDefGetter(self, "Range", "range")
    MACRO__SetupItemDefGetter(self, "ClipSize", "primary clip size")
    MACRO__SetupItemDefGetter(self, "Penetration", "penetration")
    MACRO__SetupItemDefGetter(self, "RangeModifier", "range modifier", nil, nil, 0.980000)
    MACRO__SetupItemDefGetter(self, "Bullets", "bullets", nil, nil, 1)
    MACRO__SetupItemDefGetterHasAlt(self, "TracerFreq", "tracer frequency")

    MACRO__SetupItemDefGetterHasAlt(self, "RecoilMagnitude", "recoil magnitude")

    MACRO__SetupItemDefGetter(self, "SpreadSeed", "spread seed", nil, nil, 0)

    MACRO__SetupItemDefGetter(self, "InaccuracyAltSwitch", "inaccuracy alt switch")
    MACRO__SetupItemDefGetterHasAlt(self, "Spread", "spread", nil, 0.001)
    MACRO__SetupItemDefGetterHasAlt(self, "InaccuracyMove", "inaccuracy move", nil, 0.001)
    MACRO__SetupItemDefGetterHasAlt(self, "InaccuracyLadder", "inaccuracy ladder", nil, 0.001)
    MACRO__SetupItemDefGetterHasAlt(self, "InaccuracyFire", "inaccuracy fire", nil, 0.001)
    MACRO__SetupItemDefGetterHasAlt(self, "InaccuracyReload", "inaccuracy reload", nil, 0.001)
    MACRO__SetupItemDefGetterHasAlt(self, "InaccuracyCrouch", "inaccuracy crouch", nil, 0.001)
    MACRO__SetupItemDefGetterHasAlt(self, "InaccuracyStand", "inaccuracy stand", nil, 0.001)
    MACRO__SetupItemDefGetterHasAlt(self, "InaccuracyJump", "inaccuracy jump", nil, 0.001)
    MACRO__SetupItemDefGetterHasAlt(self, "InaccuracyJumpInitial", "inaccuracy jump initial", nil, 0.001)

    --float GetInaccuracyLand ( const CEconItemView* pWepView = NULL, int nAlt = 0, float flScale = 0.001f ) const;

    MACRO__SetupItemDefGetter(self, "ZoomTime0", "zoom time 0")
    MACRO__SetupItemDefGetter(self, "ZoomFOV1", "zoom fov 1")
    MACRO__SetupItemDefGetter(self, "ZoomTime1", "zoom time 1")
    MACRO__SetupItemDefGetter(self, "ZoomFOV2", "zoom fov 2")
    MACRO__SetupItemDefGetter(self, "ZoomTime2", "zoom time 2")

    MACRO__SetupItemDefGetter(self, "IdleInterval", "idle interval", nil, nil, 20)
    MACRO__SetupItemDefGetter(self, "TimeToIdleAfterFire", "time to idle", nil, nil, 2)

    MACRO__SetupItemDefGetter(self, "RecoveryTimeStand", "recovery time stand")
    MACRO__SetupItemDefGetter(self, "RecoveryTimeStandFinal", "recovery time stand final")
    MACRO__SetupItemDefGetter(self, "RecoveryTimeCrouch", "recovery time crouch")
    MACRO__SetupItemDefGetter(self, "RecoveryTimeCrouchFinal", "recovery time crouch final")

    MACRO__SetupItemDefGetter(self, "RecoveryTransitionStartBullet", "recovery transition start bullet")
    MACRO__SetupItemDefGetter(self, "RecoveryTransitionEndBullet", "recovery transition end bullet")

    MACRO__SetupItemDefGetter(self, "CrosshairDeltaDistance", "crosshair delta distance")
    MACRO__SetupItemDefGetter(self, "CrosshairMinDistance", "crosshair min distance")

    MACRO__SetupItemDefGetter(self, "IsRevolver", "is revolver", FORCE_BOOL)
    MACRO__SetupItemDefGetter(self, "DoesUnzoomAfterShoot", "unzoom after shot", FORCE_BOOL)
    MACRO__SetupItemDefGetter(self, "HasBurstMode", "has burst mode", FORCE_BOOL)

    -- sound pitch thing that is only used by the negev
    MACRO__SetupItemDefGetter(self, "InaccuracyPitchShift", "inaccuracy pitch shift")
    MACRO__SetupItemDefGetter(self, "InaccuracyAltSoundThreshold", "inaccuracy alt sound threshold")

    self:SetWeaponType(string.lower(self.ItemVisuals.weapon_type))
    self:SetZoomOutSound(self.ItemPrefab.zoom_out_sound or "")
    self:SetZoomInSound(self.ItemPrefab.zoom_in_sound or "")
    self.SND_SINGLE = self.ItemVisuals.sound_single_shot -- default primary attack sound
    self.SND_SINGLE_ACCURATE = self.ItemVisuals.sound_single_shot_accurate -- negev uses this
    self.SND_SPECIAL1 = self.ItemVisuals.sound_special1 -- silenced weps use this
    self.SND_NEARLY_EMPTY = self.ItemVisuals.sound_nearlyempty

    -- secondary fire modes
    MACRO__SetupItemDefGetter(self, "HasSilencer", "has silencer", FORCE_BOOL)
    MACRO__SetupItemDefGetter(self, "ZoomLevels", "zoom levels")
    MACRO__SetupItemDefGetter(self, "TimeBetweenBurstShots", "time between burst shots")
    MACRO__SetupItemDefGetter(self, "CycleTimeInBurstMode", "cycletime when in burst mode")

    self.Primary.ClipSize = self.ItemAttributes["primary clip size"]
    self.Primary.Automatic = tobool(self.ItemAttributes["is full auto"])
    self.Primary.DefaultClip = self.Primary.ClipSize
    self:SetClip1(self.Primary.DefaultClip)

    self:SetSilencerOn(self:GetHasSilencer())
    self:SetWeaponMode(self:GetHasSilencer() and Secondary_Mode or Primary_Mode)

    self.m_RecoilData = {}
    self:GenerateRecoilTable(self.m_RecoilData)

    self:SetIronSightMode(IronSight_should_approach_unsighted)
    self.m_IronSightController = IronSightController(self)
    self:UpdateIronSightController()

    if self.bIsTTT then
        if self:GetWeaponType() == "pistol" then
            self.Kind = WEAPON_PISTOL
        else
            self.Kind = WEAPON_HEAVY
        end
        --SWEP.AmmoEnt = "item_ammo_pistol_ttt"
    end
end

function SWEP:UpdateIronSightController()
    if not self.m_IronSightController then
        self.m_IronSightController = IronSightController(self)
    end

    if self.m_IronSightController then
        self.m_IronSightController:Init(self)
    end
end

function SWEP:GetCycleTime()
    local flCycleTime = self:GetDefCycleTime()
    return flCycleTime ~= 0 and flCycleTime or .15 -- .15s cycle time = 400 rpm
end

-- called whenever the lua file is reloaded
function SWEP:OnReloaded()
    self:Initialize()
end

function SWEP:GetSpread()
    if self:GetWeaponMode() == Primary_Mode then
        return self:GetSpread1()
    elseif self:GetWeaponMode() == Secondary_Mode then
        return self:GetSpread2()
    else
        return 0
    end
end

function SWEP:GetHasZoom()
    return self:GetZoomLevels() ~= 0
end

function SWEP:WeaponIdle()
    if self:GetWeaponIdleTime() > CurTime() then return end

    if self:Clip1() ~= 0 then
        self:SetWeaponIdleTime(CurTime() + self:GetIdleInterval())

        -- silencers are bodygroups, so there is no longer a silencer-specific idle.
        self:SetWeaponAnim(ACT_VM_IDLE)
    end
end

function SWEP:IsUseable()
    local owner = self:GetPlayerOwner()
    if not owner then return false end

    if self:Clip1() <= 0 then
        if owner:GetAmmoCount(self:GetPrimaryAmmoType()) <= 0 and self:GetMaxClip1() ~= -1 then
            -- clip is empty ( or nonexistant ) and the player has no more ammo of this type. 
            return false
        end
    end

    return true
end

function SWEP:Think_IdleNoActions(owner)
    -- reset revolver haulback

    if CurTime() > self:GetNextPrimaryFire() and self:Clip1() == 0 and self:IsUseable() and not self:GetInReload() then
        self:Reload()
        return
    end

    self:UpdateIronSightController()
    if self:GetIronSightMode() == IronSight_viewmodel_is_deploying and self:GetActivity() ~= ACT_VM_DEPLOY then
        self:SetIronSightMode(IronSight_should_approach_unsighted)
    end

    self:WeaponIdle()
end

local activity_buts = bit.bor(IN_ATTACK, IN_ATTACK2, IN_ZOOM)
function SWEP:Think()
    local owner = self:GetPlayerOwner()
    if not owner then return end

    self:UpdateAccuracyPenalty()

    if self:GetInReload() then
        if self:GetFinishReloadTime() > CurTime() then
            self:InReloadThink()
        else
            -- the AE_WPN_COMPLETE_RELOAD event should handle the stocking the clip, but in case it's missing, we can do it here as well
            local j = math.min(self:GetClipSize() - self:Clip1(), owner:GetAmmoCount(self:GetPrimaryAmmoType()))

            -- Add them to the clip
            self:SetClip1(self:Clip1() + j)
            owner:RemoveAmmo(j, self:GetPrimaryAmmoType())

            self:SetInReload(false)
            self:OnFinishReload()
        end
    end

    if owner:KeyDown(IN_ATTACK) and self:GetNextPrimaryFire() <= CurTime() then
        -- ItemPostFrame_ProcessPrimaryAttack(owner)
    elseif owner:KeyDown(IN_ZOOM) and self:GetNextSecondaryFire() <= CurTime() then
        -- ItemPostFrame_ProcessZoomAction(owner)
    elseif owner:KeyDown(IN_ATTACK2) and self:GetNextSecondaryFire() <= CurTime() then
        -- ItemPostFrame_ProcessSecondaryAttack(owner)
    elseif owner:KeyDown(IN_RELOAD) and self:GetMaxClip1() ~= -1 and not self:GetInReload() and self:GetNextPrimaryFire() < CurTime() then
        -- ItemPostFrame_ProcessReloadAction(owner)
    elseif not owner:KeyDown(activity_buts) then
        self:Think_IdleNoActions(owner)
    end

    if self:GetIsLookingAtWeapon() and self:GetLookWeaponEndTime() <= CurTime() then
        self:StopLookingAtWeapon()
    end

    if owner:KeyDown(activity_buts) then
        self:StopLookingAtWeapon()
    end

    if owner:KeyDown(IN_ATTACK3) then
        self:TertiaryAttack()
    end

    -- GOOSEMAN : Return zoom level back to previous zoom level before we fired a shot. This is used only for the AWP.
    if self:GetResumeZoom() and self:GetNextPrimaryFire() <= CurTime()
        and self:GetZoomLevel() > 0 then -- only need to re-zoom the zoom when there's a zoom to re-zoom to. who knew?
        if self:Clip1() ~= 0 then
            self:SetWeaponMode(Secondary_Mode)
            owner:SetFOV(self:GetZoomFOV(self:GetZoomLevel()), 0.1)
        end

        owner.swcs_m_bIsScoped = true
        self:SetResumeZoom(false)
    end

    if self:GetHasBurstMode() and self:GetBurstShotsRemaining() > 0 and self:GetNextBurstShot() <= CurTime() then
        self:BurstFireRemaining()
    end

    if self:GetIsRevolver() and not owner:KeyDown(bit.bor(IN_ATTACK, IN_ATTACK2, IN_ATTACK3, IN_RELOAD)) then-- not holding any weapon buttons
        self:SetWeaponMode(Secondary_Mode)
        self:ResetPostponeFireReadyTime()
        if self:GetActivity() == ACT_VM_HAULBACK then
            self:SetWeaponAnim(ACT_VM_IDLE)
        end
    end

    self:UpdateIronSightController()
    if self:GetIronSightMode() == IronSight_viewmodel_is_deploying and self:GetActivity() ~= ACT_VM_DEPLOY then
        self:SetIronSightMode(IronSight_should_approach_unsighted)
    end
end

function SWEP:GetPlayerOwner()
    local owner = self:GetOwner()
    if not (owner:IsValid() and owner:IsPlayer()) then return false end

    return owner
end

function SWEP:TertiaryAttack()
    self:LookAtHeldWeapon()
end

function SWEP:StopLookingAtWeapon()
    self:SetIsLookingAtWeapon(false)
end

function SWEP:LookAtHeldWeapon()
    if self:GetIsLookingAtWeapon() then return end

    local nSequence = ACT_INVALID

    -- Can't taunt while zoomed, reloading, or switching silencer
    if self:IsZoomed() or self:GetInReload() or self:GetDoneSwitchingSilencer() >= CurTime() then
        return
    end

    -- don't let me inspect a shotgun that's reloading
    if self:GetWeaponType() == "shotgun" and self:GetShotgunReloadState() ~= 0 then
        return
    end

    if self.m_IronSightController and self.m_IronSightController.m_iIronSightMode == IronSight_should_approach_sighted then
        return
    end

    local vm = self:GetOwner():GetViewModel()
    if vm:IsValid() then
        nSequence = vm:SelectWeightedSequence(ACT_VM_IDLE_LOWERED)

        if nSequence == ACT_INVALID then
            nSequence = vm:LookupSequence("lookat01")
        end

        if self:GetHasSilencer() then
            self:SetBodyGroup("silencer", self:GetSilencerOn() and 0 or 1)
        end

        if nSequence ~= ACT_INVALID then
            self:SetIsLookingAtWeapon(true)
            self:SetLookWeaponEndTime(CurTime() + vm:SequenceDuration(nSequence))

            self:SetWeaponSequence(nSequence)
        end
    end
end

function SWEP:BurstFireRemaining()
    local owner = self:GetPlayerOwner()
    if not owner or self:Clip1() <= 0 then
        self:SetClip1(0)
        self:SetBurstShotsRemaining(0)
        self:SetNextBurstShot(0)
        return
    end

    self:FX_FireBullets()
    self:DoFireEffects()

    self:SetWeaponAnim(self:PrimaryAttackAct())

    owner:SetAnimation(PLAYER_ATTACK1)

    self:SetBurstShotsRemaining(self:GetBurstShotsRemaining() - 1)

    if self:GetBurstShotsRemaining() > 0 then
        self:SetNextBurstShot(CurTime() + self:GetTimeBetweenBurstShots())
    else
        self:SetNextBurstShot(0)
    end

    self:SetAccuracyPenalty(self:GetAccuracyPenalty() + self:GetInaccuracyFire())

    self:Recoil(self:GetWeaponMode())

    self:SetShotsFired(self:GetShotsFired() + 1)
    self:SetRecoilIndex(self:GetRecoilIndex() + 1)
    self:TakePrimaryAmmo(1)

    self:OnPrimaryAttack()
end

-- csgo originally has this at 1.1
-- i have it at 1.25 because some servers have low tickrate, which causes erroneous decay
local WEAPON_RECOIL_DECAY_THRESHOLD = 1.25
function SWEP:UpdateAccuracyPenalty()
    local owner = self:GetPlayerOwner()
    if not owner then return end

    local fNewPenalty = 0

    if owner:GetMoveType() == MOVETYPE_LADDER then
        fNewPenalty = fNewPenalty + self:GetInaccuracyLadder()
    elseif not owner:IsFlagSet(FL_ONGROUND) then
        fNewPenalty = fNewPenalty + self:GetInaccuracyStand()
        fNewPenalty = fNewPenalty + self:GetInaccuracyJump() * weapon_air_spread_scale:GetFloat()
    elseif owner:Crouching() then
        fNewPenalty = fNewPenalty + self:GetInaccuracyCrouch()
    else
        fNewPenalty = fNewPenalty + self:GetInaccuracyStand()
    end

    if self:GetInReload() then
        fNewPenalty = fNewPenalty + self:GetInaccuracyReload()
    end

    if fNewPenalty > self:GetAccuracyPenalty() then
        self:SetAccuracyPenalty(fNewPenalty)
    else
        local fDecayFactor = math.log(10) / self:GetRecoveryTime()
        self:SetAccuracyPenalty(Lerp(math.exp(engine.TickInterval() * -fDecayFactor), fNewPenalty, self:GetAccuracyPenalty()))
    end

    -- Decay the recoil index if a little more than cycle time has elapsed since the last shot. In other words,
    -- don't decay if we're firing full-auto.
    if SWCS_DEBUG_RECOIL_DECAY:GetBool() then
        print("should decay?", self, CurTime(), self:LastShootTime() + (self:GetCycleTime() * WEAPON_RECOIL_DECAY_THRESHOLD), CurTime() > self:LastShootTime() + (self:GetCycleTime() * WEAPON_RECOIL_DECAY_THRESHOLD))
    end
    if CurTime() > self:LastShootTime() + (self:GetCycleTime() * WEAPON_RECOIL_DECAY_THRESHOLD) then
        local fDecayFactor = math.log(10) * weapon_recoil_decay_coefficient:GetFloat()
        self:SetRecoilIndex(Lerp(math.exp(engine.TickInterval() * -fDecayFactor), 0, self:GetRecoilIndex()))
    end
end

function SWEP:GetRecoveryTime()
    local owner = self:GetPlayerOwner()
    if not owner then return -1 end

    if owner:GetMoveType() == MOVETYPE_LADDER then
        return self:GetRecoveryTimeStand()
    elseif not owner:IsFlagSet(FL_ONGROUND) then -- in air
        return self:GetRecoveryTimeCrouch()
    elseif owner:IsFlagSet(FL_DUCKING) then
        local flRecoveryTime = self:GetRecoveryTimeCrouch()
        local flRecoveryTimeFinal = self:GetRecoveryTimeCrouchFinal()

        if flRecoveryTimeFinal ~= -1 then
            local nRecoilIndex = math.floor(self:GetRecoilIndex())

            flRecoveryTime = util.RemapValClamped( nRecoilIndex, self:GetRecoveryTransitionStartBullet() or 0, self:GetRecoveryTransitionEndBullet() or 0, flRecoveryTime, flRecoveryTimeFinal )
        end

        return flRecoveryTime
    else
        local flRecoveryTime = self:GetRecoveryTimeStand()
        local flRecoveryTimeFinal = self:GetRecoveryTimeStandFinal()

        if flRecoveryTimeFinal ~= -1 then
            local nRecoilIndex = math.floor(self:GetRecoilIndex())

            flRecoveryTime = util.RemapValClamped( nRecoilIndex, self:GetRecoveryTransitionStartBullet() or 0, self:GetRecoveryTransitionEndBullet() or 0, flRecoveryTime, flRecoveryTimeFinal )
        end

        return flRecoveryTime
    end
end

local CS_PLAYER_SPEED_DUCK_MODIFIER = .34
local MOVEMENT_CURVE01_EXPONENT = .25
local weapon_accuracy_forcespread = GetConVar"weapon_accuracy_forcespread"
function SWEP:GetInaccuracy()
    local owner = self:GetPlayerOwner()
    if not owner then return 0 end
    if weapon_accuracy_nospread:GetBool() then return 0 end
    if weapon_accuracy_forcespread:GetFloat() > 0 then return weapon_accuracy_forcespread:GetFloat() end

    local fMaxSpeed = self:GetMaxSpeed()
    local fAccuracy = self:GetAccuracyPenalty()
    local flVerticalSpeed = math.abs(owner:GetVelocity().z)

    local flMovementInaccuracyScale = util.RemapValClamped(owner:GetVelocity():Length2D(),
        fMaxSpeed * CS_PLAYER_SPEED_DUCK_MODIFIER,
        fMaxSpeed * .95,
        0, 1)

    if flMovementInaccuracyScale > 0 then
        if not owner:KeyDown(IN_WALK) then
            flMovementInaccuracyScale = math.pow(flMovementInaccuracyScale, MOVEMENT_CURVE01_EXPONENT)
        end

        fAccuracy = fAccuracy + (flMovementInaccuracyScale * self:GetInaccuracyMove())
    end

    if not owner:IsFlagSet(FL_ONGROUND) then
        local flInaccuracyJumpInitial = self:GetInaccuracyJumpInitial() * weapon_air_spread_scale:GetFloat()
        local kMaxFallingPenalty = 2.0

        local fSqrtMaxJumpSpeed = math.sqrt(owner:GetJumpPower())
        local fSqrtVerticalSpeed = math.sqrt(flVerticalSpeed)

        local flAirSpeedInaccuracy = util.RemapVal(fSqrtVerticalSpeed,
            fSqrtMaxJumpSpeed * .25,
            fSqrtMaxJumpSpeed,
            0,
            flInaccuracyJumpInitial)

        if flAirSpeedInaccuracy < 0 then
            flAirSpeedInaccuracy = 0
        elseif (flAirSpeedInaccuracy > (kMaxFallingPenalty * flInaccuracyJumpInitial)) then
            flAirSpeedInaccuracy = kMaxFallingPenalty * flInaccuracyJumpInitial
        end

        fAccuracy = fAccuracy + flAirSpeedInaccuracy
    end

    if fAccuracy > 1 then
        fAccuracy = 1
    end

    return fAccuracy
end

if CLIENT then
    function SWEP:DrawWorldModel()
        if not IsValid(self.cl_wm) and self.WorldModelPlayer then
            local mdl = ClientsideModel(self.WorldModelPlayer)
            mdl:SetNoDraw(true)
            self.cl_wm = mdl
        end

        local owner = self:GetPlayerOwner()
        if IsValid(self.cl_wm) and owner then
            local mdl = self.cl_wm

            local attRH = owner:LookupAttachment("anim_attachment_RH")
            local attLH = owner:LookupAttachment("anim_attachment_LH")
            if attRH > 0 then
                local _t = owner:GetAttachment(attRH)
                local bnep,bnea = _t.Pos, _t.Ang

                _t = owner:GetAttachment(attLH)
                local lhp = _t.Pos

                local wep_attRH = mdl:LookupAttachment("weapon_hand_R")
                local wep_attLH = mdl:LookupAttachment("weapon_hand_L")
                local wrhp = Vector()
                if wep_attRH > 0 then
                    local __t = mdl:GetAttachment(wep_attRH)
                    wrhp = __t.Pos
                end

                local wlhp = Vector()
                if wep_attLH > 0 then
                    local __t = mdl:GetAttachment(wep_attLH)
                    wlhp = __t.Pos
                end

                -- calc angle so that rifles sit in left hand
                --[[if wep_attLH > 0 and attLH > 0 then
                    local dPos = (wlhp - lhp)
                    dPos:Normalize()

                    local ang = dPos:Angle()
                    ang:Normalize()

                    --bnea:Set(-ang)
                end]]

                -- position handle in right hand
                local mdlLocalPos = (wrhp - mdl:GetPos())
                bnep:Add(-mdlLocalPos)

                -- finalize
                mdl:SetPos(bnep)
                mdl:SetAngles(bnea)
                render.SetColorModulation(1, 1, 1)
                mdl:DrawModel()
                return
            end
        end

        self:DrawModel()
    end

    local viewmodel_offset_x = GetConVar"viewmodel_offset_x"
    local viewmodel_offset_y = GetConVar"viewmodel_offset_y"
    local viewmodel_offset_z = GetConVar"viewmodel_offset_z"

    function SWEP:ApplyViewModelPitchAndDip()
        --
    end

    SWEP.m_angCamDriverLastAng = Angle()
    SWEP.m_vecCamDriverLastPos = Vector()
    SWEP.m_flCamDriverAppliedTime = 0
    function SWEP:CamDriverCollect(vm)
        local iCamDriverBone = vm:LookupBone("cam_driver")
        --print('wtf', iCamDriverBone)
        if iCamDriverBone and iCamDriverBone ~= -1 then
            local bPos, bAng = vm:GetBonePosition(iCamDriverBone)

            self.m_flCamDriverAppliedTime = CurTime()
            self.m_vecCamDriverLastPos = bPos
            self.m_angCamDriverLastAng = bAng
        end
    end

    local viewmodel_recoil = GetConVar"viewmodel_recoil"
    local view_recoil_tracking = GetConVar"view_recoil_tracking"
    function SWEP:CalcViewModelView(vm, oldPos, oldAng, pos, ang)
        local ret_pos, ret_ang = oldPos * 1, oldAng * 1
        local vRight, vUp, vForward = oldAng:Right(), oldAng:Up(), oldAng:Forward()

        local ironSightController = self.m_IronSightController
        local pa = self:GetAimPunchAngle()
        if ironSightController and ironSightController:IsInIronSight() then
            local flInvIronSightAmount = ( 1.0 - ironSightController.m_flIronSightAmount )

            vForward = vForward * flInvIronSightAmount
            vUp = vUp * flInvIronSightAmount
            vRight =	vRight * flInvIronSightAmount

            pa:Normalize()
            pa = pa * math.max(flInvIronSightAmount, view_recoil_tracking:GetFloat())
            --pa:Normalize()
        end

        -- custom viewmodel offset for players
        ret_pos:Add(vForward * viewmodel_offset_y:GetFloat() + vUp * viewmodel_offset_z:GetFloat() + vRight * viewmodel_offset_x:GetFloat())

        -- little bob animation from csgo
        self:AddViewModelBob(vm, ret_pos, ret_ang)

        -- add aimpunch, viewpunch angles
        ret_ang = ret_ang + pa * viewmodel_recoil:GetFloat()
        ret_ang = ret_ang + self:GetViewPunchAngle()

        if self.m_IronSightController then
            local iron = self.m_IronSightController

            iron:ApplyIronSightPositioning(ret_pos, ret_ang)
            if iron:IsInIronSight() then
                vm:SetLocalPos(LerpVector(iron.m_flIronSightAmountGained, vm:GetLocalPos(), ret_pos))
                vm:SetLocalAngles(LerpAngle(iron.m_flIronSightAmountGained, vm:GetLocalAngles(), ret_ang))
            end
        end

        return ret_pos, ret_ang
    end

    -- Purpose: Allow the viewmodel to layer in artist-authored additive camera animation (to make some first-person anims 'punchier')
    local CAM_DRIVER_RETURN_TO_NORMAL = 0.25
    local CAM_DRIVER_RETURN_TO_NORMAL_GAIN = 0.8
    SWEP.m_flCamDriverWeight = 0
    function SWEP:CalcAddViewmodelCameraAnimation(eyeOrigin, eyeAngles)
        local owner = self:GetPlayerOwner()
        if not owner then return end

        local vm = owner:GetViewModel(self:ViewModelIndex())
        if not vm:IsValid() then return end

        local flTimeDelta = math.Clamp(CurTime() - self.m_flCamDriverAppliedTime, 0, CAM_DRIVER_RETURN_TO_NORMAL)
        --print("e", self.m_flCamDriverWeight, flTimeDelta < CAM_DRIVER_RETURN_TO_NORMAL)
        if flTimeDelta < CAM_DRIVER_RETURN_TO_NORMAL then
            self.m_flCamDriverWeight = math.Clamp(util.Gain(util.RemapValClamped(flTimeDelta, 0, CAM_DRIVER_RETURN_TO_NORMAL, 1, 0), CAM_DRIVER_RETURN_TO_NORMAL_GAIN), 0, 1)

            eyeAngles.x = (self.m_angCamDriverLastAng * self.m_flCamDriverWeight).x
            eyeAngles.y = (self.m_angCamDriverLastAng * self.m_flCamDriverWeight).y
            eyeAngles.z = (self.m_angCamDriverLastAng * self.m_flCamDriverWeight).z
        else
            self.m_flCamDriverWeight = 0
        end
        --[[
            #ifdef CLIENT_DLL
            CBaseViewModel *vm = GetViewModel();
            if ( vm && vm->GetModelPtr() )
            {
                float flTimeDelta = clamp( (gpGlobals->curtime - vm->m_flCamDriverAppliedTime), 0, CAM_DRIVER_RETURN_TO_NORMAL);
                if ( flTimeDelta < CAM_DRIVER_RETURN_TO_NORMAL )
                {
                    vm->m_flCamDriverWeight = clamp( Gain( RemapValClamped( flTimeDelta, 0.0f, CAM_DRIVER_RETURN_TO_NORMAL, 1.0f, 0.0f ), CAM_DRIVER_RETURN_TO_NORMAL_GAIN ), 0, 1 );

                    //eyeOrigin += (vm->m_vecCamDriverLastPos * vm->m_flCamDriverWeight);
                    eyeAngles += (vm->m_angCamDriverLastAng * vm->m_flCamDriverWeight);
                }
                else
                {
                    vm->m_flCamDriverWeight = 0;
                }
            }
        #endif
        ]]
    end

    function SWEP:CalcView(ply,pos,ang,fov)
        local vpang = self:GetViewPunchAngle()
        vpang:Normalize()

        local apang = self:GetAimPunchAngle()
        apang:Normalize()

        ang = ang + apang * view_recoil_tracking:GetFloat()
        ang = ang + vpang

        -- currently only used by the r8 revolver
        self:CalcAddViewmodelCameraAnimation(pos, ang)

        return pos, ang
    end

    local function VectorMA(start, scale, dir, dest)
        dest.x = start.x + scale * dir.x
        dest.y = start.y + scale * dir.y
        dest.z = start.z + scale * dir.z
    end

    local cl_bob_lower_amt = GetConVar"cl_bob_lower_amt"
    local cl_bobcycle = CreateConVar("cl_bobcycle", "0.98", FCVAR_ARCHIVE, "aaaaaaaa")
    local cl_viewmodel_shift_left_amt = CreateConVar("cl_viewmodel_shift_left_amt", "1.5", FCVAR_ARCHIVE, "eeeeeeee")
    local cl_viewmodel_shift_right_amt = CreateConVar("cl_viewmodel_shift_right_amt", "0.75", FCVAR_ARCHIVE, "eeeeeeee")
    local cl_bobup = CreateConVar("cl_bobup", "0.5", FCVAR_CHEAT, "iiiiiii")
    local cl_bobamt_vert = CreateConVar("cl_bobamt_vert", "0.25", FCVAR_ARCHIVE, "ooooooo")
    local cl_bobamt_lat = CreateConVar("cl_bobamt_lat", "0.4", FCVAR_ARCHIVE, "uuuuuu")

    local function CalcViewModelBobHelper(ply, wep)
        local bobState = wep.m_bobState
        local cycle

        if FrameTime() <= 0 or not ply:IsValid() then return 0 end

        local speed = ply:GetAbsVelocity():Length2D()

        local flSpeedFactor;
        local flRunAddAmt = 0.0
        local flmaxSpeedDelta = math.max( 0, (CurTime() - bobState.m_flLastBobTime ) * 640.0 )

        speed = math.Clamp( speed, bobState.m_flLastSpeed-flmaxSpeedDelta, bobState.m_flLastSpeed + flmaxSpeedDelta )
        speed = math.Clamp( speed, -320.0, 320.0 )

        bobState.m_flLastSpeed = speed

        if ply:GetFOV() ~= ply:GetInfoNum("fov_desired", 90) then
            flSpeedFactor = speed * 0.006
            flSpeedFactor = math.Clamp( flSpeedFactor, 0.0, 0.5 )
            local flLowerAmt = cl_bob_lower_amt:GetFloat() * 0.2

            flRunAddAmt = ( flLowerAmt * flSpeedFactor )
        end

        local bob_offset = util.RemapVal( speed, 0.0, 320.0, 0.0, 1.0 )

        bobState.m_flBobTime =  bobState.m_flBobTime + (( CurTime() - bobState.m_flLastBobTime ) * bob_offset)
        bobState.m_flLastBobTime = CurTime()

        local flBobCycle = 0.5
        local flAccuracyDiff = 0
        local flGunAccPos = 0

        if ply:IsValid() and wep:IsValid() then
            local flMaxSpeed = wep:GetMaxSpeed()
            flBobCycle = (((1000 - flMaxSpeed) / 3.5) * 0.001) * cl_bobcycle:GetFloat()

            local flAccuracy = 0.0

            local bIsElites = wep.IsElites

            if not wep:GetInReload() and not bIsElites then
                local flCrouchAccuracy = wep:GetInaccuracyCrouch()
                local flBaseAccuracy = wep:GetInaccuracyStand()
                if ply:IsFlagSet(FL_DUCKING) then
                    flAccuracy = flCrouchAccuracy
                else
                    flAccuracy = wep:GetInaccuracy()
                end

                local bIsSniper = wep:GetWeaponType() == "sniperrifle"

                local flMultiplier = 1
                if ( flAccuracy < flBaseAccuracy ) then
                    if ( not bIsSniper ) then
                        flMultiplier = 18
                    else
                        flMultiplier = 0.15
                    end

                    flMultiplier = flMultiplier * cl_viewmodel_shift_left_amt:GetFloat()
                else
                    flAccuracy = math.min( flAccuracy, 0.082 )
                    flMultiplier = flMultiplier * cl_viewmodel_shift_right_amt:GetFloat()
                end

                flAccuracyDiff = math.max( (flAccuracy - flBaseAccuracy) * flMultiplier, -0.1)
            end

            --pWeapon->m_flGunAccuracyPosition = Approach( (flAccuracyDiff*80), pWeapon->m_flGunAccuracyPosition, abs( ( (flAccuracyDiff*80)-pWeapon->m_flGunAccuracyPosition )*gpGlobals->frametime ) * 4.0f )
            --flGunAccPos = pWeapon->m_flGunAccuracyPosition
        else
            flBobCycle = (((1000.0 - 150) / 3.5) * 0.001) * cl_bobcycle:GetFloat()
        end

        cycle = bobState.m_flBobTime - math.floor(bobState.m_flBobTime / flBobCycle) * flBobCycle
        cycle = cycle / flBobCycle

        if ( cycle < cl_bobup:GetFloat() ) then
            cycle = math.pi * cycle / cl_bobup:GetFloat()
        else
            cycle = math.pi + math.pi * (cycle-cl_bobup:GetFloat()) / (1.0 - cl_bobup:GetFloat())
        end

        local flBobMultiplier = 0.00625

        if not ply:IsFlagSet(FL_ONGROUND) then
            flBobMultiplier = 0.00125
        end

        local flBobVert = bShouldIgnoreOffsetAndAccuracy and 0.3 or cl_bobamt_vert:GetFloat()
        bobState.m_flVerticalBob = speed * ( flBobMultiplier * flBobVert )
        bobState.m_flVerticalBob = ( bobState.m_flVerticalBob * 0.3 + bobState.m_flVerticalBob * 0.7 * math.sin(cycle) )
        bobState.m_flRawVerticalBob = bobState.m_flVerticalBob

        bobState.m_flVerticalBob = math.Clamp( bobState.m_flVerticalBob - ( flRunAddAmt - (flGunAccPos * 0.2) ), -7.0, 4.0 )

        cycle = bobState.m_flBobTime - math.floor(bobState.m_flBobTime / flBobCycle * 2) * flBobCycle * 2
        cycle = cycle / flBobCycle * 2

        if ( cycle < cl_bobup:GetFloat() ) then
            cycle = math.pi * cycle / cl_bobup:GetFloat()
        else
            cycle = math.pi + math.pi * (cycle - cl_bobup:GetFloat()) / (1.0 - cl_bobup:GetFloat())
        end

        local flBobLat = bShouldIgnoreOffsetAndAccuracy and 0.5 or cl_bobamt_lat:GetFloat()
        if ( ply:IsValid() and wep:IsValid() ) then
            bobState.m_flLateralBob = speed * ( flBobMultiplier * flBobLat )
            bobState.m_flLateralBob = bobState.m_flLateralBob * 0.3 + bobState.m_flLateralBob * 0.7 * math.sin(cycle)
            bobState.m_flRawLateralBob = bobState.m_flLateralBob

            bobState.m_flLateralBob = math.Clamp( bobState.m_flLateralBob + flGunAccPos * 0.25, -8.0, 8.0 )
        end
    end

    local function AddViewModelBobHelper(pos, ang, bobState)
        local vForward, vRight, vUp = ang:Forward(), ang:Right(), ang:Up()

        -- Apply bob, but scaled down to 40%
        VectorMA(pos, bobState.m_flVerticalBob * .4, vForward, pos)

        -- Z bob a bit more
        VectorMA(pos, bobState.m_flVerticalBob * .1, vUp, pos)

        -- bob the angles
        ang.r = ang.r + bobState.m_flVerticalBob * .5
        ang.p = ang.p - bobState.m_flVerticalBob * .4
        ang.y = ang.y - bobState.m_flLateralBob * .3

        VectorMA(pos, bobState.m_flLateralBob * 0.2, vRight, pos)
    end

    function SWEP:AddViewModelBob(vm, pos, ang)
        self.m_bobState = self.m_bobState or {
            m_flBobTime = 0,
            m_flLastBobTime = 0,
            m_flLastSpeed = 0,
            m_flVerticalBob = 0,
            m_flLateralBob = 0,
            m_flRawVerticalBob = 0,
            m_flRawLateralBob = 0,
        }

        local owner = self:GetPlayerOwner()
        if not owner then return end

        CalcViewModelBobHelper(owner, self)
        AddViewModelBobHelper(pos, ang, self.m_bobState)
    end

    SWEP.m_fAnimInset = 0
    SWEP.m_fLineSpreadDistance = 0

    local cl_crosshair_sniper_show_normal_inaccuracy = CreateClientConVar("cl_crosshair_sniper_show_normal_inaccuracy", "0", nil, nil, "Include standing inaccuracy when determining sniper crosshair blur")
    local cl_crosshair_sniper_width = GetConVar"cl_crosshair_sniper_width"

    local matDust = Material"overlays/scope_lens_csgo"
    local matArc  = Material"sprites/scope_arc_csgo"
    local matBlur = Material"sprites/scope_line_blur"

    function SWEP:DrawHUD()
        local owner = self:GetPlayerOwner()
        if not owner then return end

        self:DrawCrosshair()

        if self:GetWeaponType() ~= "sniperrifle" then return end

        local kScopeMinFOV = 25.0 -- Clamp scope FOV to this value to prevent blur from getting too big when double-scoped
        local flTargetFOVForZoom = math.max( self:GetZoomFOV( self:GetZoomLevel() ), kScopeMinFOV )

        if flTargetFOVForZoom ~= owner:GetInfoNum("fov_default", 90) and self:IsZoomed() and not self:GetResumeZoom() then
            local screenWide, screenTall = ScrW(), ScrH()

            local vm = owner:GetViewModel(0)
            if not vm:IsValid() then return end

            local fHalfFov = math.rad(flTargetFOVForZoom) * 0.5
            local fInaccuracyIn640x480Pixels = 320.0 / math.tan( fHalfFov ) -- 640 = "reference screen width"

            -- Get the weapon's inaccuracy
            local fWeaponInaccuracy = self:GetInaccuracy() + self:GetSpread()

            -- Optional: Ignore "default" inaccuracy
            if not cl_crosshair_sniper_show_normal_inaccuracy:GetBool() then
                fWeaponInaccuracy = fWeaponInaccuracy - self:GetInaccuracyStand(Secondary_Mode) + self:GetSpread()
            end

            fWeaponInaccuracy = math.max( fWeaponInaccuracy, 0 )

            local fRawSpreadDistance = fWeaponInaccuracy * fInaccuracyIn640x480Pixels
            local fSpreadDistance = math.Clamp( fRawSpreadDistance, 0, 100 )

            -- reduce the goal  (* 0.4 / 30.0f)
            -- then animate towards it at speed 19.0f
            -- (where did these numbers come from?)
            local flInsetGoal = fSpreadDistance * (0.4 / 30.0);
            self.m_fAnimInset = math.Approach(self.m_fAnimInset, flInsetGoal, math.abs((flInsetGoal - self.m_fAnimInset) - FrameTime()) * 19)

            -- Approach speed chosen so we get 90% there in 3 frames if we are running at 192 fps vs a 64tick client/server.
            -- If our fps is lower we will reach the target faster, if higher it is slightly slower
            -- (since this is a framerate-dependent approach function).
            self.m_fLineSpreadDistance = util.RemapValClamped(FrameTime() * 140, 0, 1, self.m_fLineSpreadDistance, fRawSpreadDistance)

            local flAccuracyFishtail = self:GetAccuracyFishtail()
            local offsetX = math.floor(self.m_bobState.m_flRawLateralBob * (screenTall / 14) + flAccuracyFishtail)
            local offsetY = math.floor(self.m_bobState.m_flRawVerticalBob * (screenTall / 14))

            local flInacDisplayBlur = self.m_fAnimInset * 0.04
            if flInacDisplayBlur > 0.22 then
                flInacDisplayBlur = 0.22
            end

            -- calculate the bounds in which we should draw the scope
            local inset = math.floor((screenTall / 14) + (flInacDisplayBlur * (screenTall * 0.5)))
            local y1 = inset
            local x1 = (screenWide - screenTall) / 2 + inset
            local y2 = screenTall - inset
            local x2 = screenWide - x1

            y1 = y1 + offsetY
            y2 = y2 + offsetY
            x1 = x1 + offsetX
            x2 = x2 + offsetX

            local x = (screenWide / 2) + offsetX
            local y = (screenTall / 2) + offsetY

            local uv1 = 0.5 / 256
            local uv2 = 1 - uv1

            local vert = {{}, {}, {}, {}}

            local uv11 = Vector(uv1, uv1, 0)
            local uv12 = Vector(uv1, uv2, 0)
            local uv21 = Vector(uv2, uv1, 0)
            local uv22 = Vector(uv2, uv2, 0)

            local xMod = (screenWide / 2) + offsetX + (flInacDisplayBlur * screenTall)
            local yMod = (screenTall / 2) + offsetY + (flInacDisplayBlur * screenTall)

            local iMiddleX = (screenWide / 2) + offsetX
            local iMiddleY = (screenTall / 2) + offsetY

            surface.SetMaterial(matDust)
            surface.SetDrawColor(255, 255, 255, 200)

            vert[1].x = iMiddleX + xMod
            vert[1].y = iMiddleY + yMod
            vert[1].u = uv21.x
            vert[1].v = uv21.y

            vert[2].x = iMiddleX - xMod
            vert[2].y = iMiddleY + yMod
            vert[2].u = uv11.x
            vert[2].v = uv11.y

            vert[3].x = iMiddleX - xMod
            vert[3].y = iMiddleY - yMod
            vert[3].u = uv12.x
            vert[3].v = uv12.y

            vert[4].x = iMiddleX + xMod
            vert[4].y = iMiddleY - yMod
            vert[4].u = uv22.x
            vert[4].v = uv22.y

            surface.DrawPoly(vert)

            -- The math.pow here makes the blur not quite spread out quite as much as the actual inaccuracy;
            -- doing so is a bit too sudden and also leads to just a huge blur because the snipers are
            -- *extremely* inaccurate while scoped and moving.  This way we get a slightly smoother animation
            -- as well as not quite blowing up the blurred area by such a large amount.
            local fBlurWidth = math.pow(self.m_fLineSpreadDistance, 0.75)
            local fScreenBlurWidth = fBlurWidth * screenTall / 640.0  -- scale from 'reference screen size' to actual screen

            local nSniperCrosshairThickness = cl_crosshair_sniper_width:GetInt()
            if nSniperCrosshairThickness < 1 then
                nSniperCrosshairThickness = 1
            end

            local kMaxVarianceWithFullAlpha = 1.8 -- Tuned to look good
            local fBlurAlpha
            if fScreenBlurWidth <= nSniperCrosshairThickness + 0.5 then
                fBlurAlpha = (fBlurWidth < 1) and 1 or 1 / fBlurWidth
            else
                fBlurAlpha = (fBlurWidth < kMaxVarianceWithFullAlpha) and 1 or kMaxVarianceWithFullAlpha / fBlurWidth
            end

            -- This is a break from physical reality to make the look a bit better.  An actual Gaussian
            -- blur spreads the energy out over the entire blurred area, dropping the total opacity by the amount
            -- of the spread.  However, this leads to not being able to see the effect at all.  We solve this in 
            -- 2 ways:
            --   (1) use sqrt on the alpha to bring it closer to 1, kind of like a gamma curve.
            --   (2) clamp the alpha at the lower end to 55% to make sure you can see *something* no matter
            --       how spread out it gets.
            fBlurAlpha = math.sqrt(fBlurAlpha)
            local iBlurAlpha = math.floor(math.Clamp(fBlurAlpha * 255, 140, 255))

            -- //DevMsg( "blur: %8.5f fov: %8.5f alpha: %8.5f\n", fBlurWidth, flTargetFOVForZoom, fBlurAlpha );

            if fScreenBlurWidth <= nSniperCrosshairThickness + 0.5 then
                surface.SetDrawColor(0, 0, 0, iBlurAlpha)

                -- Draw the reticle with primitives
                if nSniperCrosshairThickness <= 1 then
                    surface.DrawLine(0, y, screenWide + offsetX, y)
                    surface.DrawLine(x, 0, x, screenTall + offsetY)
                else
                    local nStep = math.floor(nSniperCrosshairThickness / 2)
                    surface.DrawRect(0, y - nStep, screenWide + offsetX, y + nSniperCrosshairThickness - nStep)
                    surface.DrawRect(x - nStep, 0, x + nSniperCrosshairThickness - nStep, screenTall + offsetY)
                end
            else
                surface.SetDrawColor(0, 0, 0, iBlurAlpha)
                surface.SetMaterial(matBlur)

                -- vertical blurred line
                vert[1].x = iMiddleX - fScreenBlurWidth
                vert[1].y = offsetY
                vert[1].u = uv11.x
                vert[1].v = uv11.y

                vert[2].x = iMiddleX + fScreenBlurWidth
                vert[2].y = offsetY
                vert[2].u = uv21.x
                vert[2].v = uv21.y

                vert[3].x = iMiddleX + fScreenBlurWidth
                vert[3].y = screenTall + offsetY
                vert[3].u = uv22.x
                vert[3].v = uv22.y

                vert[4].x = iMiddleX - fScreenBlurWidth
                vert[4].y = screenTall + offsetY
                vert[4].u = uv12.x
                vert[4].v = uv12.y
                surface.DrawPoly(vert)

                -- horizontal blurred line
                vert[1].x = screenWide + offsetX
                vert[1].y = iMiddleY - fScreenBlurWidth
                vert[1].u = uv12.x
                vert[1].v = uv12.y

                vert[2].x = screenWide + offsetX
                vert[2].y = iMiddleY + fScreenBlurWidth
                vert[2].u = uv22.x
                vert[2].v = uv22.y

                vert[3].x = offsetX
                vert[3].y = iMiddleY + fScreenBlurWidth
                vert[3].u = uv21.x
                vert[3].v = uv21.y

                vert[4].x = offsetX
                vert[4].y = iMiddleY - fScreenBlurWidth
                vert[4].u = uv11.x
                vert[4].v = uv11.y
                surface.DrawPoly(vert)
            end

            surface.SetDrawColor(0, 0, 0, 255)
            surface.SetMaterial(matArc)

            -- bottom right
            vert[1].x = x
            vert[1].y = y
            vert[1].u = uv11.x
            vert[1].v = uv11.y

            vert[2].x = x2
            vert[2].y = y
            vert[2].u = uv21.x
            vert[2].v = uv21.y

            vert[3].x = x2
            vert[3].y = y2
            vert[3].u = uv22.x
            vert[3].v = uv22.y

            vert[4].x = x
            vert[4].y = y2
            vert[4].u = uv12.x
            vert[4].v = uv12.y
            surface.DrawPoly(vert)

            -- top right
            vert[1].x = x - 1
            vert[1].y = y1
            vert[1].u = uv12.x
            vert[1].v = uv12.y

            vert[2].x = x2
            vert[2].y = y1
            vert[2].u = uv22.x
            vert[2].v = uv22.y

            vert[3].x = x2
            vert[3].y = y + 1
            vert[3].u = uv21.x
            vert[3].v = uv21.y

            vert[4].x = x - 1
            vert[4].y = y + 1
            vert[4].u = uv11.x
            vert[4].v = uv11.y
            surface.DrawPoly(vert)

            -- bottom left
            vert[1].x = x1
            vert[1].y = y
            vert[1].u = uv21.x
            vert[1].v = uv21.y

            vert[2].x = x
            vert[2].y = y
            vert[2].u = uv11.x
            vert[2].v = uv11.y

            vert[3].x = x
            vert[3].y = y2
            vert[3].u = uv12.x
            vert[3].v = uv12.y

            vert[4].x = x1
            vert[4].y = y2
            vert[4].u = uv22.x
            vert[4].v = uv22.y
            surface.DrawPoly(vert)

            -- top left
            vert[1].x = x1
            vert[1].y = y1
            vert[1].u = uv22.x
            vert[1].v = uv22.y

            vert[2].x = x
            vert[2].y = y1
            vert[2].u = uv12.x
            vert[2].v = uv12.y

            vert[3].x = x
            vert[3].y = y
            vert[3].u = uv11.x
            vert[3].v = uv11.y

            vert[4].x = x1
            vert[4].y = y
            vert[4].u = uv21.x
            vert[4].v = uv21.y
            surface.DrawPoly(vert)

            surface.DrawRect(0, 0, screenWide, y1) -- top
            surface.DrawRect(0, y2, screenWide, screenTall) -- bottom
            surface.DrawRect(0, y1, x1, screenTall) -- left
            surface.DrawRect(x2, y1, screenWide, screenTall) -- right
        end
    end

    local cl_crosshairdot = GetConVar"cl_crosshairdot"
    local cl_crosshairstyle = GetConVar"cl_crosshairstyle"
    local cl_crosshaircolor = GetConVar"cl_crosshaircolor"
    local cl_crosshairalpha = GetConVar"cl_crosshairalpha"
    local cl_crosshaircolor_r = GetConVar"cl_crosshaircolor_r"
    local cl_crosshaircolor_g = GetConVar"cl_crosshaircolor_g"
    local cl_crosshaircolor_b = GetConVar"cl_crosshaircolor_b"
    local cl_crosshair_dynamic_splitdist = GetConVar"cl_crosshair_dynamic_splitdist"
    local cl_crosshairgap_useweaponvalue = GetConVar"cl_crosshairgap_useweaponvalue"
    local cl_crosshairgap = GetConVar"cl_crosshairgap"
    local cl_crosshairsize = GetConVar"cl_crosshairsize"
    local cl_crosshairthickness = GetConVar"cl_crosshairthickness"
    local cl_crosshair_dynamic_splitalpha_innermod = GetConVar"cl_crosshair_dynamic_splitalpha_innermod"
    local cl_crosshair_dynamic_splitalpha_outermod = GetConVar"cl_crosshair_dynamic_splitalpha_outermod"
    local cl_crosshair_dynamic_maxdist_splitratio = GetConVar"cl_crosshair_dynamic_maxdist_splitratio"
    local cl_crosshair_drawoutline = GetConVar"cl_crosshair_drawoutline"
    local cl_crosshair_outlinethickness = GetConVar"cl_crosshair_outlinethickness"
    local cl_crosshairusealpha = GetConVar"cl_crosshairusealpha"

    local function DrawCrosshairRect(r,g,b,a, x0, y0, x1, y1, bAdditive)
        local w = math.max(x0, x1) - math.min(x0, x1)
        local h = math.max(y0, y1) - math.min(y0, y1)

        if cl_crosshair_drawoutline:GetBool() then
            local flThick = cl_crosshair_outlinethickness:GetFloat()
            surface.SetDrawColor(0,0,0,a)
            surface.DrawRect(x0 - math.floor(flThick / 2), y0 - math.floor(flThick / 2), w + flThick, h + flThick)
        end

        surface.SetDrawColor(r,g,b,a)

        if bAdditive then
            surface.DrawTexturedRect(x0, y0, w, h)
        else
            surface.DrawRect(x0, y0, w, h)
        end
    end

    local SWITCH_CrosshairColor = {
        [0] = Color(250, 50, 50),
        Color(50, 250, 50),
        Color(250, 250, 50),
        Color(50, 50, 250),
        Color(50, 250, 250),
        function ()
            return Color(
                cl_crosshaircolor_r:GetInt(),
                cl_crosshaircolor_g:GetInt(),
                cl_crosshaircolor_b:GetInt()
            )
        end,
    }

    -- #define XRES(x)	( x  * ( ( float )ScreenWidth() / 640.0 ) )
    -- #define YRES(y)	( y  * ( ( float )ScreenHeight() / 480.0 ) )
    local function XRES(x)
        return x * (ScrW() / 640)
    end
    local function YRES(y)
        return y * (ScrH() / 480)
    end

    local cl_crosshair_t = GetConVar"cl_crosshair_t"

    SWEP.m_flCrosshairDistance = 0
    SWEP.m_iAmmoLastCheck = 0
    function SWEP:DrawCrosshair()
        local owner = self:GetPlayerOwner()
        if not owner then return end

        assert(owner == LocalPlayer() or LocalPlayer():GetObserverMode() == OBS_MODE_IN_EYE)

        -- no crosshair for sniper rifles

        -- localplayer must be owner if not in Spec mode

        local r,g,b = 50, 250, 50
        if SWITCH_CrosshairColor[cl_crosshaircolor:GetInt()] then
            local col = SWITCH_CrosshairColor[cl_crosshaircolor:GetInt()]

            if isfunction(col) then
                col = col()
            end

            r,g,b = col.r, col.g, col.b
        end

        local alpha = math.Clamp(cl_crosshairalpha:GetInt(), 0, 255)

        if not self.m_iCrosshairTextureID then
            self.m_iCrosshairTextureID = surface.GetTextureID("vgui/white_additive")
        end

        local bAdditive = not cl_crosshairusealpha:GetBool()
        if bAdditive then
            surface.SetTexture(self.m_iCrosshairTextureID)
            alpha = 200
        end

        local iron = self.m_IronSightController
        if iron and iron:ShouldHideCrossHair() then
            alpha = 0
        end

        if self:GetWeaponType() == "sniperrifle" then
            return true
        end

        local fHalfFov = math.rad(owner:GetFOV()) * 0.5
        local flInaccuracy = self:GetInaccuracy()
        local flSpread = self:GetSpread()

        local fSpreadDistance = ((flInaccuracy + flSpread) * 320 / math.tan(fHalfFov))
        local flCappedSpreadDistance = fSpreadDistance
        local flMaxCrossDistance = cl_crosshair_dynamic_splitdist:GetFloat()
        if fSpreadDistance > flMaxCrossDistance then
            flCappedSpreadDistance = flMaxCrossDistance
        end

        local iSpreadDistance = cl_crosshairstyle:GetInt() < 4 and math.floor(YRES(fSpreadDistance)) or 2
        local iCappedSpreadDistance = cl_crosshairstyle:GetInt() < 4 and math.floor(YRES(flCappedSpreadDistance)) or 2

        local flAccuracyFishtail = self:GetAccuracyFishtail()

        local iDeltaDistance = self:GetCrosshairDeltaDistance() -- amount by which the crosshair expands when shooting (per frame)
        local fCrosshairDistanceGoal = cl_crosshairgap_useweaponvalue:GetBool() and self:GetCrosshairMinDistance() or 4 -- The minimum distance the crosshair can achieve...

        -- 0 = default
        -- 1 = default static
        -- 2 = classic standard
        -- 3 = classic dynamic
        -- 4 = classic static
        -- if ( cl_dynamiccrosshair.GetBool() )
        if cl_crosshairstyle:GetInt() ~= 4 and (cl_crosshairstyle:GetInt() == 2 or cl_crosshairstyle:GetInt() == 3) then
            if not owner:IsOnGround() then
                fCrosshairDistanceGoal = fCrosshairDistanceGoal * 2
            elseif owner:Crouching() then
                fCrosshairDistanceGoal = fCrosshairDistanceGoal * 0.5
            elseif owner:GetVelocity():Length() > 100 then
                fCrosshairDistanceGoal = fCrosshairDistanceGoal * 1.5
            end
        end

        -- [jpaquin] changed to only bump up the crosshair size if the player is still shooting or is spectating someone else
        if self:GetShotsFired() > self.m_iAmmoLastCheck and (owner:KeyDown(IN_ATTACK) or owner:KeyDown(IN_ATTACK2)) and self:Clip1() >= 0 then
            if cl_crosshairstyle:GetInt() == 5 then
                self.m_flCrosshairDistance = self.m_flCrosshairDistance + (self:GetRecoilMagnitude(self:GetWeaponMode()) / 3.5)
            elseif cl_crosshairstyle:GetInt() ~= 4 then
                fCrosshairDistanceGoal = fCrosshairDistanceGoal + iDeltaDistance
            end
        end

        self.m_iAmmoLastCheck = self:GetShotsFired()

        if self.m_flCrosshairDistance > fCrosshairDistanceGoal then
            if cl_crosshairstyle:GetInt() == 5 then
                self.m_flCrosshairDistance = self.m_flCrosshairDistance - 42 * FrameTime()
            else
                self.m_flCrosshairDistance = Lerp(FrameTime() / 0.025, fCrosshairDistanceGoal, self.m_flCrosshairDistance)
            end
        end

        -- clamp max crosshair expansion
        self.m_flCrosshairDistance = math.Clamp(self.m_flCrosshairDistance, fCrosshairDistanceGoal, 25.0)

        local iCrosshairDistance, iBarSize, iBarThickness
        local iCappedCrosshairDistance = 0

        iCrosshairDistance = math.floor((self.m_flCrosshairDistance * ScrH() / 1200.0) + cl_crosshairgap:GetFloat())
        iBarSize = math.floor( YRES( cl_crosshairsize:GetFloat() ))
        iBarThickness = math.max( 1, math.floor( YRES( cl_crosshairthickness:GetFloat() )) )

        -- 0 = default
        -- 1 = default static
        -- 2 = classic standard
        -- 3 = classic dynamic
        -- 4 = classic static
        -- if weapon_debug_spread_show:GetInt() == 2
        if iSpreadDistance > 0 and cl_crosshairstyle:GetInt() == 2 or cl_crosshairstyle:GetInt() == 3 then
            iCrosshairDistance = iSpreadDistance + cl_crosshairgap:GetFloat()

            if cl_crosshairstyle:GetInt() == 2 then
                iCappedCrosshairDistance = iCappedSpreadDistance + cl_crosshairgap:GetFloat()
            end
        elseif cl_crosshairstyle:GetInt() == 4 or (iSpreadDistance == 0 and (cl_crosshairstyle:GetInt() == 2 or cl_crosshairstyle:GetInt() == 3)) then
            iCrosshairDistance = fCrosshairDistanceGoal + cl_crosshairgap:GetFloat()
            iCappedCrosshairDistance = 4 + cl_crosshairgap:GetFloat()
        end

        local iCenterX = math.floor(ScrW() / 2)
        local iCenterY = math.floor(ScrH() / 2)

        local flAngleToScreenPixel = 0

        --[[
            // subtract a ratio of cam driver motion from crosshair according to cl_cam_driver_compensation_scale
            if ( cl_cam_driver_compensation_scale.GetFloat() != 0 )
            {
                CBasePlayer *pOwner = ToBasePlayer( GetPlayerOwner() );
                if ( pOwner )
                {
                    CBaseViewModel *vm = pOwner->GetViewModel( m_nViewModelIndex );
                    if ( vm && vm->m_flCamDriverWeight > 0 )
                    {
                        QAngle angCamDriver = vm->m_flCamDriverWeight * vm->m_angCamDriverLastAng * clamp( cl_cam_driver_compensation_scale.GetFloat(), -10.0f, 10.0f );
                        if ( angCamDriver.x != 0 || angCamDriver.y != 0  )
                        {
                            flAngleToScreenPixel = VIEWPUNCH_COMPENSATE_MAGIC_SCALAR * 2 * ( ScreenHeight() / ( 2.0f * tanf(DEG2RAD( pPlayer->GetFOV() ) / 2.0f) ) );
                            iCenterY -= ( flAngleToScreenPixel * sinf( DEG2RAD( angCamDriver.x ) ) ) ;
                            iCenterX += ( flAngleToScreenPixel * sinf( DEG2RAD( angCamDriver.y ) ) ) ;
                        }
                    }
                }
            }

            /*
                // Optionally subtract out viewangle since it doesn't affect shooting.
                if ( cl_flinch_compensate_crosshair.GetBool() )
                {
                    QAngle viewPunch = pPlayer->GetViewPunchAngle();

                    if ( viewPunch.x != 0 || viewPunch.y != 0 )
                    {
                        if ( flAngleToScreenPixel == 0 )
                            flAngleToScreenPixel = VIEWPUNCH_COMPENSATE_MAGIC_SCALAR * 2 * ( ScreenHeight() / ( 2.0f * tanf(DEG2RAD( pPlayer->GetFOV() ) / 2.0f) ) );

                        iCenterY -= flAngleToScreenPixel * sinf( DEG2RAD( viewPunch.x ) );
                        iCenterX += flAngleToScreenPixel * sinf( DEG2RAD( viewPunch.y ) );
                    }
                }
            */
        ]]

        local nFishTaleShift = (flAccuracyFishtail * (ScrW() / 500))

        -- 0 = default
        -- 1 = default static
        -- 2 = classic standard
        -- 3 = classic dynamic
        -- 4 = classic static
        if cl_crosshairstyle:GetInt() == 4 then
            nFishTaleShift = 0
        end

        local flAlphaSplitInner = cl_crosshair_dynamic_splitalpha_innermod:GetFloat()
        local flAlphaSplitOuter = cl_crosshair_dynamic_splitalpha_outermod:GetFloat()
        local flSplitRatio = cl_crosshair_dynamic_maxdist_splitratio:GetFloat()
        local iInnerCrossDist = iCrosshairDistance
        local flLineAlphaInner = alpha
        local flLineAlphaOuter = alpha
        local iBarSizeInner = iBarSize
        local iBarSizeOuter = iBarSize

        -- draw the crosshair that splits off from the main xhair
        if cl_crosshairstyle:GetInt() == 2 and fSpreadDistance > flMaxCrossDistance then
            iInnerCrossDist = iCappedCrosshairDistance
            flLineAlphaInner = alpha * flAlphaSplitInner
            flLineAlphaOuter = alpha * flAlphaSplitOuter
            iBarSizeInner = math.ceil(iBarSize * (1.0 - flSplitRatio))
            iBarSizeOuter = math.floor(iBarSize * flSplitRatio)

            -- draw horizontal crosshair lines
            local iInnerLeft = (iCenterX - iCrosshairDistance - iBarThickness / 2 + nFishTaleShift) - iBarSizeInner
            local iInnerRight = iInnerLeft + 2 * (iCrosshairDistance + iBarSizeInner) + iBarThickness + nFishTaleShift
            local iOuterLeft = iInnerLeft - iBarSizeOuter
            local iOuterRight = iInnerRight + iBarSizeOuter
            local y0 = iCenterY - iBarThickness / 2
            local y1 = y0 + iBarThickness
            DrawCrosshairRect(r, g, b, flLineAlphaOuter, iOuterLeft, y0, iInnerLeft, y1, bAdditive)
            DrawCrosshairRect(r, g, b, flLineAlphaOuter, iInnerRight, y0, iOuterRight, y1, bAdditive)

            -- draw vertical crosshair lines
            local iInnerTop = (iCenterY - iCrosshairDistance - iBarThickness / 2) - iBarSizeInner
            local iInnerBottom = iInnerTop + 2 * (iCrosshairDistance + iBarSizeInner) + iBarThickness
            local iOuterTop = iInnerTop - iBarSizeOuter
            local iOuterBottom = iInnerBottom + iBarSizeOuter
            local x0 = iCenterX - iBarThickness / 2
            local x1 = x0 + iBarThickness
            if not cl_crosshair_t:GetBool() then DrawCrosshairRect(r, g, b, flLineAlphaOuter, x0, iOuterTop, x1, iInnerTop, bAdditive) end
            DrawCrosshairRect(r, g, b, flLineAlphaOuter, x0, iInnerBottom, x1, iOuterBottom, bAdditive)
        end

        -- draw horizontal crosshair lines
        local iInnerLeft = iCenterX - iInnerCrossDist - (iBarThickness / 2) + nFishTaleShift
        local iInnerRight = iInnerLeft + (2 * iInnerCrossDist) + iBarThickness + nFishTaleShift
        local iOuterLeft = iInnerLeft - iBarSizeInner
        local iOuterRight = iInnerRight + iBarSizeInner
        local y0 = iCenterY - (iBarThickness / 2)
        local y1 = y0 + iBarThickness
        DrawCrosshairRect(r, g, b, flLineAlphaInner, iOuterLeft, y0, iInnerLeft, y1, bAdditive)
        DrawCrosshairRect(r, g, b, flLineAlphaInner, iInnerRight, y0, iOuterRight, y1, bAdditive)

        -- draw vertical crosshair lines
        local iInnerTop = iCenterY - iInnerCrossDist - (iBarThickness / 2)
        local iInnerBottom = iInnerTop + (2 * iInnerCrossDist) + iBarThickness
        local iOuterTop = iInnerTop - iBarSizeInner
        local iOuterBottom = iInnerBottom + iBarSizeInner
        local x0 = iCenterX - (iBarThickness / 2)
        local x1 = x0 + iBarThickness
        if not cl_crosshair_t:GetBool() then DrawCrosshairRect(r, g, b, flLineAlphaInner, x0, iOuterTop, x1, iInnerTop, bAdditive) end
        DrawCrosshairRect(r, g, b, flLineAlphaInner, x0, iInnerBottom, x1, iOuterBottom, bAdditive)

        -- draw dot
        if cl_crosshairdot:GetBool() then
            local x0 = iCenterX - iBarThickness / 2
            local x1 = x0 + iBarThickness
            local y0 = iCenterY - iBarThickness / 2
            local y1 = y0 + iBarThickness
            DrawCrosshairRect(r, g, b, alpha, x0, y0, x1, y1, bAdditive)
        end
        --[[
            if ( weapon_debug_spread_show.GetInt() == 1 )
            {
                // colors
                r = 250;
                g = 250;
                b = 50;
                //vgui::surface()->DrawSetColor( r, g, b, alpha );

                int iBarThickness = MAX( 1, RoundFloatToInt( YRES( cl_crosshairthickness.GetFloat() )) );

                // draw vertical spread lines
                int iInnerLeft	= iCenterX - iSpreadDistance;
                int iInnerRight	= iCenterX + iSpreadDistance;
                int iOuterLeft	= iInnerLeft - iBarThickness;
                int iOuterRight	= iInnerRight + iBarThickness;
                int iInnerTop		= iCenterY - iSpreadDistance;
                int iInnerBottom	= iCenterY + iSpreadDistance;
                int iOuterTop		= iInnerTop - iBarThickness;
                int iOuterBottom	= iInnerBottom + iBarThickness;

                int iGap = RoundFloatToInt( weapon_debug_spread_gap.GetFloat() * iSpreadDistance );

                // draw horizontal lines
                DrawCrosshairRect( r, g, b, alpha, iOuterLeft, iOuterTop, iCenterX - iGap, iInnerTop, bAdditive );
                DrawCrosshairRect( r, g, b, alpha, iCenterX + iGap, iOuterTop, iOuterRight, iInnerTop, bAdditive );
                DrawCrosshairRect( r, g, b, alpha, iOuterLeft, iInnerBottom, iCenterX - iGap, iOuterBottom, bAdditive );
                DrawCrosshairRect( r, g, b, alpha, iCenterX + iGap, iInnerBottom, iOuterRight, iOuterBottom, bAdditive );

                // draw vertical lines
                DrawCrosshairRect( r, g, b, alpha, iOuterLeft, iOuterTop, iInnerLeft, iCenterY - iGap, bAdditive );
                DrawCrosshairRect( r, g, b, alpha, iOuterLeft, iCenterY + iGap, iInnerLeft, iOuterBottom, bAdditive );
                DrawCrosshairRect( r, g, b, alpha, iInnerRight, iOuterTop, iOuterRight, iCenterY - iGap, bAdditive );
                DrawCrosshairRect( r, g, b, alpha, iInnerRight, iCenterY + iGap, iOuterRight, iOuterBottom, bAdditive );
            }
        ]]

        return true
    end
    function SWEP:DoDrawCrosshair() return true end
end

local AE_CL_BODYGROUP_SET_VALUE = 39
local AE_CL_BODYGROUP_SET_TO_CLIP = 41
local AE_CL_BODYGROUP_SET_TO_NEXTCLIP = 42
local AE_CL_HIDE_SILENCER = 43
local AE_CL_ATTACH_SILENCER_COMPLETE = 44
local AE_CL_SHOW_SILENCER = 45
local AE_CL_DETACH_SILENCER_COMPLETE = 46
local AE_WPN_PRIMARYATTACK = 49
local AE_WPN_COMPLETE_RELOAD = 54
local AE_BEGIN_TAUNT_LOOP = 72
local AE_CL_SET_STATTRAK_GLOW = 73
local AE_WPN_CZ_DUMP_CURRENT_MAG = 74
local AE_WPN_CZ_UPDATE_BODYGROUP = 75
local AE_MUZZLEFLASH = 5001

local SILENCER_VISIBLE = 0
local SILENCER_HIDDEN = 1

function SWEP:GetReloadActivity()
    return self.m_iReloadActivityIndex or ACT_VM_RELOAD
end

SWEP.VM_BodyGroups = {}
function SWEP:SetBodyGroup(bodygroup, p_value)
    local owner = self:GetPlayerOwner()
    if not owner then return end

    local vm = owner:GetViewModel(self:ViewModelIndex())
    if not vm:IsValid() then return end

    local index, value = nil, tonumber(p_value)
    if string.find(bodygroup, " ") then
        index = tonumber(bodygroup:match"^(.+)%s%d")
        value = bodygroup:match("%s(%d+)$")
    end

    if not index then
        index = vm:FindBodygroupByName(bodygroup)
    end
    value = tonumber(value)

    if not (isnumber(index) and isnumber(value)) then return end

    self.VM_BodyGroups[index] = value
    if SERVER then
        self:CallOnClient("SetBodyGroup", Format("%s %s", index, value))
    end
end

function SWEP:PreDrawViewModel(vm, _, owner)
    for id, val in next, self.VM_BodyGroups do
        vm:SetBodygroup(id, val)
    end
end

local SWITCH_ANIMEVENT = {
    [AE_BEGIN_TAUNT_LOOP] = function(self, _, _, options)
        local owner = self:GetPlayerOwner()
        if not owner then return end

        local vm = owner:GetViewModel(self:ViewModelIndex())
        if not vm:IsValid() then return end

        options = tonumber(options)

        -- when gmod lets me :SetCycle() on VMs, i will finish this
        -- homonovus, 08/12/2020

        -- pViewModel->ForceCycle( 0 );
        -- pViewModel->ResetSequence( nSequence );

        --vm:SetCycle(options)
        --vm:ResetSequence(vm:GetSequence())
        --vm:SetCycle(options)
        --print(self, "cycle", vm, options, vm:GetCycle())
    end,
    [AE_CL_BODYGROUP_SET_VALUE] = function(self, _, _, options)
        options = options:Split" "
        local bodygroup = options[1]
        local value = tonumber(options[2])

        if not IsValid(self) then return end
        local owner = self:GetPlayerOwner()
        if not owner then return end

        self:SetBodyGroup(bodygroup, value)
    end,
    [AE_WPN_PRIMARYATTACK] = function(self, _, _, options)
        local time = tonumber(options)
        if time then
            self:SetPostponeFireReadyTime(CurTime() + time)

            -- send everyone else the "click" back noise
            -- except in eye observers
            self:EmitSound("Weapon_Revolver_CSGO.Prepare")
        end
    end,
    [AE_WPN_CZ_DUMP_CURRENT_MAG] = function(self)
        -- csgo used to empty the mag when you reloaded the cz??!?!
        -- self:SetClip1(0)
        self:SetBodyGroup("front_mag", 1)

        local vm = self:GetOwner():GetViewModel(self:ViewModelIndex())
        -- if the front mag is removed, all subsequent anims use the non-front mag reload
        self.m_iReloadActivityIndex = vm:GetSequenceActivity(vm:LookupSequence("reload2"))

        -- lua: cz is the only thing that uses this, so i'm just gonna...
        -- as opposed to checking inside :Deploy() whether it has ammo or not
        self.m_bAlreadyReloaded = true
    end,
    [AE_CL_BODYGROUP_SET_TO_CLIP] = function(self)
        local owner = self:GetPlayerOwner()
        if not owner then return end

        local vm = owner:GetViewModel(self:ViewModelIndex())
        if not vm:IsValid() then return end

        for i = 0, vm:GetNumBodyGroups() - 1 do
            self:SetBodyGroup(vm:GetBodygroupName(i), (self:Clip1() < i) and 1 or 0)
        end
    end,
    [AE_CL_BODYGROUP_SET_TO_NEXTCLIP] = function(self)
        local owner = self:GetPlayerOwner()
        local vm = owner:GetViewModel(self:ViewModelIndex())

        local iNextClip = math.min( self:GetMaxClip1(), self:Clip1() + owner:GetAmmoCount(self:GetPrimaryAmmoType()))
        for i = 0, vm:GetNumBodyGroups() - 1 do
            self:SetBodyGroup(vm:GetBodygroupName(i), (iNextClip >= i) and 0 or 1)
        end
    end,
    [AE_CL_HIDE_SILENCER] = function(self)
        self:SetBodyGroup("silencer", SILENCER_HIDDEN)
    end,
    [AE_CL_SHOW_SILENCER] = function(self)
        self:SetBodyGroup("silencer", SILENCER_VISIBLE)
    end,
    [AE_WPN_COMPLETE_RELOAD] = function(self)
        local owner = self:GetPlayerOwner()
        if not owner then return end

        self.m_bReloadVisuallyComplete = true
        local j = math.min(self:GetClipSize() - self:Clip1(), owner:GetAmmoCount(self:GetPrimaryAmmoType()))

        self:SetClip1(self:Clip1() + j)
        owner:RemoveAmmo(j, self:GetPrimaryAmmoType())

        self:SetRecoilIndex(0)
    end,
    [AE_CL_DETACH_SILENCER_COMPLETE] = function(self)
        self:SetWeaponMode(Primary_Mode)
        self:SetSilencerOn(false)
    end,
    [AE_CL_ATTACH_SILENCER_COMPLETE] = function(self)
        self:SetWeaponMode(Secondary_Mode)
        self:SetSilencerOn(true)
    end,
    [AE_MUZZLEFLASH] = function(self, _, _, options)
        if self:GetSilencerOn() then
            return true
        end

        local data = EffectData()
        data:SetFlags( 0 )
        data:SetEntity( self:GetPlayerOwner():GetViewModel(self:ViewModelIndex()) )
        data:SetAttachment( tonumber(options) )
        data:SetScale( 1 )

        --print(self.ItemVisuals["muzzle_flash_effect_1st_person"])

        util.Effect( "CS_MuzzleFlash", data )
        return true
    end
}

function SWEP:FireAnimationEvent(pos, ang, event, options, src_ent)
    if SWCS_DEBUG_AE:GetBool() then
        print("csgo AE", self, event, options, src_ent)
    end

    if SWITCH_ANIMEVENT[event] then
        local fn = SWITCH_ANIMEVENT[event]
        local ret = fn(self,pos,ang,options,src_ent)

        if ret ~= nil then
            return ret
        end
    end
end

-- sandbox lets ppl deploy weps at 4x speed
-- sometimes servers will lower this
local cl_crosshairstyle = GetConVar"cl_crosshairstyle"
function SWEP:Deploy()
    self:SetHoldType(self.HoldType)

    if CLIENT and (cl_crosshairstyle:GetInt() == 4 or cl_crosshairstyle:GetInt() == 5) then
        self.m_flCrosshairDistance = 1
    end

    self:SetRecoilIndex(0)

    if self:GetHasZoom() then
        self:SetZoomLevel(0)
        self:SetWeaponMode(Primary_Mode)
    end

    if self:GetSilencerOn() then
        self:SetWeaponAnim(ACT_VM_DRAW_SILENCED, self:GetDeploySpeed())
    elseif self.m_bAlreadyReloaded then -- cz alt draw anim; i do it like this instead of checking if they have ammo
        local seq = self:LookupSequence("draw2")
        local act

        if seq then
            act = self:GetSequenceActivity(seq)
            self:SetWeaponAnim(act, self:GetDeploySpeed())
        end

        self:SetBodyGroup("front_mag", 1)
    end

    local owner = self:GetPlayerOwner()
    if owner then
        local vm = owner:GetViewModel(self:ViewModelIndex())
        if vm:IsValid() then
            vm:SetPlaybackRate(self:GetDeploySpeed())
            self:SetWeaponIdleTime(CurTime() + (self:SequenceDuration() * (1 / self:GetDeploySpeed())))

            local oPrim = self:GetNextPrimaryFire()
            local oSec = self:GetNextSecondaryFire()
            self:SetNextPrimaryFire(oPrim < self:GetWeaponIdleTime() and self:GetWeaponIdleTime() or oPrim)
            self:SetNextSecondaryFire(oSec < self:GetWeaponIdleTime() and self:GetWeaponIdleTime() or oSec)
        end
    end

    self:SetIronSightMode(IronSight_viewmodel_is_deploying)
    if self.m_IronSightController then
        self.m_IronSightController:SetState(IronSight_viewmodel_is_deploying)
    end

    if self:GetIsRevolver() then
        self:SetWeaponMode(Secondary_Mode)
    end

    return true
end

function SWEP:DoFireEffects()
    if self:GetSilencerOn() then
        return
    end

    local owner = self:GetPlayerOwner()
    if owner then
        owner:MuzzleFlash()
    end
end

function SWEP:CalculateNextAttackTime(fCycleTime)
    local fCurAttack = self:GetNextPrimaryFire()
    local fDeltaAttack = CurTime() - fCurAttack
    if fDeltaAttack < 0 or fDeltaAttack > engine.TickInterval() then
        fCurAttack = CurTime()
    end
    self:SetNextPrimaryFire(fCurAttack + fCycleTime)
    self:SetNextSecondaryFire(fCurAttack + fCycleTime)

    return fCurAttack
end

function SWEP:IsPistol()
    return self:GetWeaponType() == "pistol"
end

function SWEP:PlayEmptySound()
    if self:IsPistol() then
        self:EmitSound("Default.ClipEmpty_Pistol")
    else
        self:EmitSound("Default.ClipEmpty_Rifle")
    end
end

function SWEP:GetTracerFrequency(weaponMode)
    if weaponMode == Primary_Mode then
        return self:GetTracerFreq1()
    else
        return self:GetTracerFreq2()
    end
end

function SWEP:PrimaryAttackAct()
    return ACT_VM_PRIMARYATTACK
end

local MaxPitchShiftInaccuracy = 0.05
local weapon_near_empty_sound = GetConVar"weapon_near_empty_sound"
function SWEP:FX_FireBullets()
    local owner = self:GetPlayerOwner()
    if not owner then return end

    local iSeed = self:GetRandomSeed()
    iSeed = iSeed + 1
    self:SetRandomSeed(iSeed)

    local fInaccuracy = self:GetInaccuracy()
    local soundToPlay = self:GetHasSilencer() and self:GetSilencerOn() and self.SND_SPECIAL1 or self.SND_SINGLE

    local flPitchShift = self:GetInaccuracyPitchShift() * (fInaccuracy < MaxPitchShiftInaccuracy and fInaccuracy or MaxPitchShiftInaccuracy)
    if soundToPlay == self.SND_SINGLE and self:GetInaccuracyAltSoundThreshold() > 0 and fInaccuracy < self:GetInaccuracyAltSoundThreshold() then
        soundToPlay = self.SND_SINGLE_ACCURATE
        flPitchShift = 0
    end
    self:EmitSound(soundToPlay, nil, 100 + math.floor(flPitchShift))

    -- If the gun's nearly empty, also play a subtle "nearly-empty" sound, since the weapon 
    -- is lighter and acoustically different when weighed down by fewer bullets.
    -- But really it's so you get a fun low ammo warning from an audio cue.
    if weapon_near_empty_sound:GetBool() and
        self:GetMaxClip1() > 1 and -- not a single-shot weapon
        (self:Clip1() / self:GetMaxClip1()) <= 0.2 -- 20% or fewer bullets remaining
    then
        self:EmitSound(self.SND_NEARLY_EMPTY)
    end

    local bForceMaxInaccuracy = false
    local bForceInaccuracyDirection = false

    local rand = UniformRandomStream(iSeed) -- init random system with this seed

    -- Accuracy curve density adjustment FOR R8 REVOLVER SECONDARY FIRE, NEGEV WILD BEAST
    local flRadiusCurveDensity = rand:RandomFloat()
    if self.IsR8Revolver and self:GetWeaponMode() == Secondary_Mode then -- R8 REVOLVER SECONDARY FIRE
        flRadiusCurveDensity = 1 - (flRadiusCurveDensity * flRadiusCurveDensity)
    elseif self.IsNegev and self:GetRecoilIndex() < 3 then -- NEGEV WILD BEAST
        for j = 3, self:GetRecoilIndex(), -1 do
            flRadiusCurveDensity = flRadiusCurveDensity * flRadiusCurveDensity
        end

        flRadiusCurveDensity = 1 - flRadiusCurveDensity
    end

    if bForceMaxInaccuracy then
        flRadiusCurveDensity = 1
    end

    -- Get accuracy displacement
    local fTheta0 = rand:RandomFloat(0, 2 * math.pi)
    if bForceInaccuracyDirection then
        fTheta0 = math.pi * 0.5
    end

    local fRadius0 = flRadiusCurveDensity * fInaccuracy
    local x0 = fRadius0 * math.cos(fTheta0)
    local y0 = fRadius0 * math.sin(fTheta0)

    local x1, y1 = {}, {}
    assert(self:GetBullets() <= 16, "too many bullets in weapon")

    for iBullet = 1, self:GetBullets() do
        local flSpreadCurveDensity = rand:RandomFloat()
        if self:GetIsRevolver() and self:GetWeaponMode() == Secondary_Mode then
            flSpreadCurveDensity = 1 - (flSpreadCurveDensity * flSpreadCurveDensity)
        elseif self.IsNegev and self:GetRecoilIndex() < 3 then
            for j = 3, self:GetRecoilIndex(), -1 do
                flSpreadCurveDensity = flSpreadCurveDensity * flSpreadCurveDensity
            end

            flSpreadCurveDensity = 1 - flSpreadCurveDensity
        end

        if bForceMaxInaccuracy then
            flSpreadCurveDensity = 1
        end

        local fTheta1 = rand:RandomFloat(0, 2 * math.pi)
        if bForceInaccuracyDirection then
            fTheta1 = math.pi * .5
        end

        local fRadius1 = flSpreadCurveDensity * self:GetSpread()
        x1[iBullet] = fRadius1 * math.cos(fTheta1)
        y1[iBullet] = fRadius1 * math.sin(fTheta1)
    end

    local angShooting = owner:GetAimVector():Angle() + self:GetAimPunchAngle()
    angShooting:Normalize()

    local vecDirShooting, vecRight, vecUp = angShooting:Forward(), angShooting:Right(), angShooting:Up()

    -- fire bullets individually to avoid getting shotguns clipped to bbox
    for iBullet = 1, self:GetBullets() do
        local xSpread, ySpread = x0 + x1[iBullet], y0 + y1[iBullet]

        local vecDir = vecDirShooting + (xSpread * vecRight) + (ySpread * vecUp)
        vecDir:Normalize()

        owner:FireBullets({
            Src = owner:GetShootPos(),
            Dir = vecDir,
            Num = 1,
            AmmoType = self.Primary.Ammo,
            Tracer = self:GetTracerFrequency(self:GetWeaponMode()),
            TracerName = self.Tracer,
            Attacker = owner,
            Distance = self:GetRange(),
            Spread = vector_origin,
            Damage = self:GetDamage(),
            Callback = function(atk, tr, dmg)
                if SERVER or (CLIENT and IsFirstTimePredicted()) then
                    self:BulletCallback(atk, tr, dmg, vecDir)
                end
            end
        })
    end
end

-- head = 4x; chest & arms = 1x; stomach = 1.25x; legs = .75x
local HITGROUP_DAMAGE_SCALE = {
    [HITGROUP_HEAD] = 4,
    [HITGROUP_STOMACH] = 1.25,
    [HITGROUP_CHEST] = 1,
    [HITGROUP_LEFTARM] = 1,
    [HITGROUP_RIGHTARM] = 1,
    [HITGROUP_LEFTLEG] = .75,
    [HITGROUP_RIGHTLEG] = .75
}
function SWEP:ApplyDamageScale(dmgInfo, tr, flBaseDamage)

    -- rescale damage bc gmod has its own scalar
    -- we are trying to recreate csgo weps and how ppl expect them to be :)
    if HITGROUP_DAMAGE_SCALE[tr.HitGroup] then
        dmgInfo:SetDamage(flBaseDamage)
        dmgInfo:ScaleDamage(HITGROUP_DAMAGE_SCALE[tr.HitGroup])
    end
end

local ironsight_rand = UniformRandomStream()

function SWEP:CSBaseGunFire(flCycleTime, weaponMode)
    local owner = self:GetPlayerOwner()
    if not owner then
        return false end

    if self:Clip1() <= 0 then
        self:PlayEmptySound()
        self:SetNextPrimaryFire(CurTime() + .2)

        if self:GetIsRevolver() then
            self:SetNextPrimaryFire(CurTime() + self:GetCycleTime(weaponMode))
            self:SetNextSecondaryFire(CurTime() + self:GetCycleTime(weaponMode))
            self:SetWeaponAnim(ACT_VM_DRYFIRE)
        end

        return false
    end

    if (self:GetWeaponType() ~= "sniperrifle" and self:IsZoomed()) or (self:GetIsRevolver() and weaponMode == Secondary_Mode) then
        self:SetWeaponAnim(ACT_VM_SECONDARYATTACK)
    elseif self:GetIsRevolver() then
        self:SetWeaponAnim(self:PrimaryAttackAct())
    else
        self:SetWeaponAnim(self:PrimaryAttackAct())
    end

    owner:SetAnimation(PLAYER_ATTACK1)

    self:FX_FireBullets()
    self:DoFireEffects()

    local iron = self.m_IronSightController
    if iron then
        iron:IncreaseDotBlur(ironsight_rand:RandomFloat(.22, .28))
    end

    self:SetWeaponIdleTime(CurTime() + self:GetTimeToIdleAfterFire())

    self:SetAccuracyPenalty(self:GetAccuracyPenalty() + self:GetInaccuracyFire())

    self:Recoil(self:GetWeaponMode())

    self:SetShotsFired(self:GetShotsFired() + 1)
    self:SetRecoilIndex(self:GetRecoilIndex() + 1)
    self:TakePrimaryAmmo(1)

    self:OnPrimaryAttack()

    self:SetNextPrimaryFire(self:CalculateNextAttackTime(flCycleTime))
    self:SetNextSecondaryFire(self:CalculateNextAttackTime(flCycleTime))
    self:SetLastShootTime()

    return true
end

function SWEP:ResetPostponeFireReadyTime()
    self:SetPostponeFireReadyTime(math.huge)
end

function SWEP:PrimaryAttack()
    local owner = self:GetPlayerOwner()
    if not owner then return end

    if self:GetIsRevolver() then -- holding primary, will fire when time is elapsed
        -- don't allow a rapid fire shot instantly in the middle of a haul back hold, let the hammer return first
        self:SetNextSecondaryFire(CurTime() + .25)
        if self:GetActivity() ~= ACT_VM_HAULBACK then
            self:SetWeaponAnim(ACT_VM_HAULBACK)
            self:ResetPostponeFireReadyTime()
            return
        end

        self:SetWeaponMode(Primary_Mode)

        if self:GetPostponeFireReadyTime() > CurTime() then return end

        --if ( m_bFireOnEmpty )
        --{
        --  ResetPostponeFireReadyTime();
        --  m_flNextPrimaryAttack = m_flNextSecondaryAttack = gpGlobals->curtime + 0.5f;
        --}

        -- we're going to fire after this point
    end

    -- cant shoot underwater thing

    local flCycleTime = self:GetCycleTime()

    -- change a few things if we're in burst mode
    if self:GetBurstMode() then
        flCycleTime = self:GetCycleTimeInBurstMode()
        self:SetBurstShotsRemaining(2)
        self:SetNextBurstShot(CurTime() + self:GetTimeBetweenBurstShots())
    end

    if not self:CSBaseGunFire(flCycleTime, self:GetWeaponMode()) then
        return
    end

    if self:GetSilencerOn() then
        self:SetWeaponAnim(ACT_VM_SECONDARYATTACK)
    end

    if self:IsZoomed() and self:GetDoesUnzoomAfterShoot() then
        owner.swcs_m_bIsScoped = false
        self:SetResumeZoom(true)
        owner:SetFOV(0, 0.05)
        self:SetWeaponMode(Primary_Mode)
    end
end

function SWEP:IsZoomed()
    return self:GetZoomLevel() > 0
end

function SWEP:GetZoomFOV(iZoomLevel)
    if iZoomLevel == 0 then return 0 -- not used, always return default FOV
    elseif iZoomLevel == 1 then return self:GetZoomFOV1()
    elseif iZoomLevel == 2 then return self:GetZoomFOV2()
    else return 0 end
end
function SWEP:GetZoomTime(iZoomLevel)
    if iZoomLevel == 0 then return self:GetZoomTime0()
    elseif iZoomLevel == 1 then return self:GetZoomTime1()
    elseif iZoomLevel == 2 then return self:GetZoomTime2()
    else return 0 end
end

function SWEP:SecondaryAttack()
    if self:GetNextPrimaryFire() >= CurTime() then return end
    local owner = self:GetPlayerOwner()
    if not owner then return end

    if self:GetHasZoom() then
        self:SetZoomLevel(self:GetZoomLevel() + 1)
        if self:GetZoomLevel() > self:GetZoomLevels() then
            self:SetZoomLevel(0)
        end

        if self:IsZoomed() then
            self:EmitSound(self:GetZoomInSound())

            owner.swcs_m_bIsScoped = true
            self:SetWeaponMode(Secondary_Mode)
            owner:SetFOV(self:GetZoomFOV(self:GetZoomLevel()), self:GetZoomTime(self:GetZoomLevel()))
            self:SetAccuracyPenalty(self:GetAccuracyPenalty() + self:GetInaccuracyAltSwitch())

            if self.m_IronSightController and self.m_IronSightController.m_pAttachedWeapon:IsValid() then
                self:UpdateIronSightController()
                owner:SetFOV(self.m_IronSightController.m_flIronSightFOV, self.m_IronSightController:GetIronSightPullUpDuration())
                self.m_IronSightController:SetState(IronSight_should_approach_sighted)

                self:StopLookingAtWeapon()

                -- force idle
                self:SetWeaponAnim(ACT_VM_IDLE)
            end
        else
            self:EmitSound(self:GetZoomOutSound())

            owner.swcs_m_bIsScoped = false
            self:SetWeaponMode(Primary_Mode)
            owner:SetFOV(0, self:GetZoomTime(0))
            self:SetWeaponAnim(ACT_VM_FIDGET)

            if self.m_IronSightController then
                self.m_IronSightController:SetState(IronSight_should_approach_unsighted)
            end
        end
    elseif self:GetHasSilencer() and self:GetDoneSwitchingSilencer() <= CurTime() then
        if self:GetSilencerOn() then
            self:SetWeaponAnim(ACT_VM_DETACH_SILENCER)
        else
            self:SetWeaponAnim(ACT_VM_ATTACH_SILENCER)
        end

        local nextAttackTime = CurTime() + self:SequenceDuration()
        self:SetDoneSwitchingSilencer(nextAttackTime)
        self:SetNextPrimaryFire(nextAttackTime)
        self:SetNextSecondaryFire(nextAttackTime)
    elseif self:GetHasBurstMode() then
        if self:GetBurstMode() then
            owner:PrintMessage(HUD_PRINTCENTER, "auto")
            self:SetBurstMode(false)
            self:SetWeaponMode(Primary_Mode)
        else
            owner:PrintMessage(HUD_PRINTCENTER, "burst")
            self:SetBurstMode(true)
            self:SetWeaponMode(Secondary_Mode)
        end

        self:EmitSound("Weapon.AutoSemiAutoSwitch")
    elseif self:GetIsRevolver() and self:GetNextSecondaryFire() < CurTime() then
        local flCycletimeAlt = self:GetCycleTime( Secondary_Mode );
        self:SetWeaponMode(Secondary_Mode)
        self:UpdateAccuracyPenalty()

        self:CSBaseGunFire( flCycletimeAlt, Secondary_Mode )
        self:SetNextSecondaryFire(CurTime() + flCycletimeAlt)
        return
    end
end

function SWEP:OnStartReload()
    local owner = self:GetPlayerOwner()

    if self:GetZoomLevel() > 0 and owner.swcs_m_bIsScoped then
        owner:SetFOV(0, 0)
        owner.swcs_m_bIsScoped = false
    end

    if self:GetHasZoom() then
        self:SetZoomLevel(0)
        self:SetResumeZoom(false)
        self:SetWeaponMode(Primary_Mode)
    end

    self.m_bReloadVisuallyComplete = false
    self:SetIronSightMode(IronSight_should_approach_unsighted)

    self:SetShotsFired(0)
    self:SetRecoilIndex(self:GetRecoilIndex() + 1)
end

function SWEP:Holster()
    -- silencer stuff here
    if (self:GetActivity() == ACT_VM_ATTACH_SILENCER and not self:GetSilencerOn()) or
        (self:GetActivity() == ACT_VM_DETACH_SILENCER and self:GetSilencerOn())
    then
        self:SetDoneSwitchingSilencer(CurTime())
        self:SetNextPrimaryFire(CurTime())
        self:SetNextSecondaryFire(CurTime())
    end

    if self:GetHasZoom() then
        self:SetZoomLevel(0)
        self:SetWeaponMode(Primary_Mode)
    end

    -- animation cancel for unfinished reload
    if self:GetInReload() and not self.m_bReloadVisuallyComplete then
        self:SetNextPrimaryFire(CurTime())
        self:SetNextSecondaryFire(CurTime())
    end

    -- lua: reset bodygroups bc gmod doesnt
    -- :Holster() is only called when owner is valid
    local owner = self:GetPlayerOwner()
    if owner then
        local vm = owner:GetViewModel()
        if vm:IsValid() then
            for i = 0, vm:GetNumBodyGroups() - 1 do
                vm:SetBodygroup(i, 0)
            end
        end
    end

    if self.m_IronSightController then
        self.m_IronSightController:SetState(IronSight_viewmodel_is_deploying)
    end

    return BaseClass.Holster(self)
end

local weapon_recoil_suppression_shots = CreateConVar("weapon_recoil_suppression_shots", "4")
local weapon_recoil_suppression_factor = CreateConVar("weapon_recoil_suppression_factor", "0.75")
local weapon_recoil_variance = CreateConVar("weapon_recoil_variance", "0.55")
function SWEP:GenerateRecoilTable(data)
    local iSuppressionShots = weapon_recoil_suppression_shots:GetInt()
    local fBaseSuppressionFactor = weapon_recoil_suppression_factor:GetFloat()
    local fRecoilVariance = weapon_recoil_variance:GetFloat()
    local recoilRandom = UniformRandomStream()

    if not data then return end

    local iSeed = 0
    local bHasAttrSeed = false
    local bFullAuto = false
    local bHasAttrFullAuto = false
    local flRecoilAngle = {}
    local bHasAttrRecoilAngle = {}
    local flRecoilAngleVariance = {}
    local bHasAttrRecoilAngleVariance = {}
    local flRecoilMagnitude = {}
    local bHasAttrRecoilMagnitude = {}
    local flRecoilMagnitudeVariance = {}
    local bHasAttrRecoilMagnitudeVariance = {}

    if self.ItemAttributes then
        iSeed = tonumber(self.ItemAttributes["recoil seed"])
        bFullAuto = tobool(self.ItemAttributes["is full auto"])
        for iMode = 0, 1 do
            local isAlt = iMode == 1 and " alt" or ""

            flRecoilAngle[iMode] = self.ItemAttributes["recoil angle" .. isAlt]
            bHasAttrRecoilAngle[iMode] = self.ItemAttributes["recoil angle" .. isAlt] ~= nil
            flRecoilAngleVariance[iMode] = self.ItemAttributes["recoil angle variance" .. isAlt]
            bHasAttrRecoilAngleVariance[iMode] = self.ItemAttributes["recoil angle variance" .. isAlt] ~= nil
            flRecoilMagnitude[iMode] = self.ItemAttributes["recoil magnitude" .. isAlt]
            bHasAttrRecoilMagnitude[iMode] = self.ItemAttributes["recoil magnitude" .. isAlt] ~= nil
            flRecoilMagnitudeVariance[iMode] = self.ItemAttributes["recoil magnitude variance" .. isAlt]
            bHasAttrRecoilMagnitudeVariance[iMode] = self.ItemAttributes["recoil magnitude variance" .. isAlt] ~= nil
        end

        bHasAttrSeed = iSeed ~= nil
        bHasAttrFullAuto = bFullAuto ~= nil
    end

    for iMode = 0, 1 do
        data[iMode] = data[iMode] or {}
        assert(bHasAttrSeed, "no recoil seed attribute")
        assert(bHasAttrFullAuto, "no full auto attribute")

        flRecoilAngle[iMode] = flRecoilAngle[iMode] or 0
        assert(bHasAttrRecoilAngleVariance[iMode], Format("no recoil angle variance attribute on iMode: %d", iMode))
        assert(bHasAttrRecoilMagnitude[iMode], Format("no recoil magnitude attribute on iMode: %d", iMode))
        assert(bHasAttrRecoilMagnitudeVariance[iMode], Format("no recoil magnitude variance attribute on iMode: %d", iMode))

        recoilRandom:SetSeed(iSeed)
        local fAngle = 0
        local fMagnitude = 0

        -- data->recoilTable[64] has 64 elements; [0-63]
        -- start it at 0 just for solidarity
        for j = 0, 63 do
            data[iMode][j] = data[iMode][j] or {}

            local fAngleNew = flRecoilAngle[iMode] + recoilRandom:RandomFloat(- flRecoilAngleVariance[iMode], flRecoilAngleVariance[iMode] );
            local fMagnitudeNew = flRecoilMagnitude[iMode] + recoilRandom:RandomFloat(- flRecoilMagnitudeVariance[iMode], flRecoilMagnitudeVariance[iMode] );

            if ( bFullAuto and ( j > 0 ) ) then
                fAngle = Lerp( fRecoilVariance, fAngle, fAngleNew )
                fMagnitude = Lerp( fRecoilVariance, fMagnitude, fMagnitudeNew )
            else
                fAngle = fAngleNew
                fMagnitude = fMagnitudeNew
            end

            if ( bFullAuto and ( j < iSuppressionShots ) ) then
                local fSuppressionFactor = Lerp( j / iSuppressionShots, fBaseSuppressionFactor, 1.0 );
                fMagnitude = fMagnitude * fSuppressionFactor;
            end

            data[iMode][j].fAngle = fAngle;
            data[iMode][j].fMagnitude = fMagnitude;
        end
    end
end

function SWEP:GetRecoilOffset(iMode, iIndex)
    if not self.m_RecoilData or table.IsEmpty(self.m_RecoilData) then
        ErrorNoHalt("Generating recoil table too late")

        self.m_RecoilData = {}
        self:GenerateRecoilTable(self.m_RecoilData)
    end

    iIndex = iIndex % 63

    local elem = self.m_RecoilData[iMode][iIndex]
    if elem then
        return elem.fAngle, elem.fMagnitude
    else
        return 0, 0
    end
end

local weapon_recoil_view_punch_extra = GetConVar"weapon_recoil_view_punch_extra"
function SWEP:Recoil(iMode)
    local owner = self:GetPlayerOwner()
    if not owner then return end

    if SWCS_DEBUG_RECOIL:GetBool() then
        print(Format("recoiling on %s index: %f", self, self:GetRecoilIndex()))
    end

    local iIndex = math.floor(self:GetRecoilIndex())
    local fAngle, fMagnitude = self:GetRecoilOffset(iMode, iIndex)

    local angleVel = Angle()
    angleVel.y = -math.sin(math.rad(fAngle)) * fMagnitude
    angleVel.p = -math.cos(math.rad(fAngle)) * fMagnitude
    angleVel = angleVel + self:GetAimPunchAngleVel()
    self:SetAimPunchAngleVel(angleVel)

    -- this bit gives additional punch to the view (screen shake) to make the kick back a bit more visceral
    local viewPunch = self:GetViewPunchAngle()
    local fViewPunchMagnitude = fMagnitude * weapon_recoil_view_punch_extra:GetFloat()
    viewPunch.y = viewPunch.y - math.sin(math.rad(fAngle)) * fViewPunchMagnitude
    viewPunch.p = viewPunch.p - math.cos(math.rad(fAngle)) * fViewPunchMagnitude
    viewPunch:Normalize()

    self:SetViewPunchAngle(viewPunch)
end

-- decay angles in PlayerMove()
function SWEP:OnMove(move, cmd)
    self:DecayViewPunchAngle()
    self:DecayAimPunchAngle()
end

local function DecayAngles(v, fExp, fLin, dT)
    v = Vector(v.x, v.y, v.z)

    fExp = fExp * dT
    fLin = fLin * dT

    v = v * math.exp(-fExp)

    local fMag = v:Length()
    if fMag > fLin then
        v = v * (1 - fLin / fMag)
    else
        v = vector_origin
    end

    v = Angle(v.x, v.y, v.z)

    return v
end

local view_punch_decay = CreateConVar("view_punch_decay", "18", nil, "Decay factor exponent for view punch")
function SWEP:DecayViewPunchAngle()
    local punchAng = self:GetViewPunchAngle()
    punchAng:Normalize()
    local decayedAng = DecayAngles(punchAng, view_punch_decay:GetFloat(), 0, engine.TickInterval())
    decayedAng:Normalize()

    self:SetViewPunchAngle(decayedAng)
end

local weapon_recoil_decay2_exp = CreateConVar("weapon_recoil_decay2_exp", "8", nil, "Decay factor exponent for weapon recoil")
local weapon_recoil_decay2_lin = CreateConVar("weapon_recoil_decay2_lin", "18", nil, "Decay factor (linear term) for weapon recoil")
local weapon_recoil_vel_decay = CreateConVar("weapon_recoil_vel_decay", "4.5", nil, "Decay factor for weapon recoil velocity")
function SWEP:DecayAimPunchAngle()
    local punchAng = self:GetRawAimPunchAngle()
    local punchAngVel = self:GetAimPunchAngleVel()
    punchAng:Normalize()
    punchAngVel:Normalize()

    local new = DecayAngles(punchAng, weapon_recoil_decay2_exp:GetFloat(), weapon_recoil_decay2_lin:GetFloat(), engine.TickInterval())
    punchAng = new + (punchAngVel * engine.TickInterval() * .5)

    punchAngVel = punchAngVel * math.exp(engine.TickInterval() * -weapon_recoil_vel_decay:GetFloat())

    punchAng = punchAng + (punchAngVel * engine.TickInterval() * .5)

    punchAng:Normalize()
    punchAngVel:Normalize()

    self:SetRawAimPunchAngle(punchAng)
    self:SetAimPunchAngleVel(punchAngVel)
end

local SWITCH_BulletTypeParameters = {
    ["BULLET_PLAYER_50AE"] = {
        fPenetrationPower = 30,
        flPenetrationDistance = 1000,
    },
    ["BULLET_PLAYER_762MM"] = {
        fPenetrationPower = 39,
        flPenetrationDistance = 5000,
    },
    ["BULLET_PLAYER_556MM"] = {
        fPenetrationPower = 35,
        flPenetrationDistance = 4000,
    },
    ["BULLET_PLAYER_338MAG"] = {
        fPenetrationPower = 45,
        flPenetrationDistance = 8000,
    },
    ["BULLET_PLAYER_9MM"] = {
        fPenetrationPower = 21,
        flPenetrationDistance = 800,
    },
    ["BULLET_PLAYER_BUCKSHOT"] = {
        fPenetrationPower = 0,
        flPenetrationDistance = 0,
    },
    ["BULLET_PLAYER_45ACP"] = {
        fPenetrationPower = 15,
        flPenetrationDistance = 500,
    },
    ["BULLET_PLAYER_357SIG"] = {
        fPenetrationPower = 25,
        flPenetrationDistance = 800,
    },
    ["BULLET_PLAYER_57MM"] = {
        fPenetrationPower = 30,
        flPenetrationDistance = 2000,
    },
    ["AMMO_TYPE_TASERCHARGE"] = {
        fPenetrationPower = 0,
        flPenetrationDistance = 0,
    },
}
SWITCH_BulletTypeParameters["BULLET_PLAYER_556MM_SMALL"] = SWITCH_BulletTypeParameters["BULLET_PLAYER_556MM"]
SWITCH_BulletTypeParameters["BULLET_PLAYER_556MM_BOX"] = SWITCH_BulletTypeParameters["BULLET_PLAYER_556MM"]
SWITCH_BulletTypeParameters["BULLET_PLAYER_357SIG_SMALL"] = SWITCH_BulletTypeParameters["BULLET_PLAYER_357SIG"]
SWITCH_BulletTypeParameters["BULLET_PLAYER_357SIG_P250"] = SWITCH_BulletTypeParameters["BULLET_PLAYER_357SIG"]
SWITCH_BulletTypeParameters["BULLET_PLAYER_357SIG_MIN"] = SWITCH_BulletTypeParameters["BULLET_PLAYER_357SIG"]

local DEFAULT_PENETRATE = {
    penetrationmodifier = 1.0,
    damagemodifier = 0.5,
}
local pen_types = {}
local function new(new, base, penmod, dmgmod)
    new = string.lower(new)
    if base then base = string.lower(base) end

    pen_types[new] = pen_types[base] or table.Copy(DEFAULT_PENETRATE)

    if penmod then pen_types[new].penetrationmodifier = penmod end
    if dmgmod then pen_types[new].damagemodifier = dmgmod end
end
new("default")
new("default_silent")
new("rock")
new("solidmetal", nil, 0.3, 0.27)
new("metal", "solidmetal", 0.4)
new("dirt", nil, 0.3, 0.6) new("grass", "dirt")
new("plaster", "dirt", 0.6, 0.7)
new("concrete", nil, 0.25, 0.5)
new("flesh", nil, 0.9)
new("alienflesh", "flesh")
new("plastic_barrel", nil, 0.7)
new("glass", nil, 0.99)
new("metalpanel", "metal", 0.45, 0.5)
new("Plastic_Box", nil, 0.75)
new("plastic", "Plastic_Box")
new("Wood", nil, 0.6, 0.9)
new("combine_metal", "metal")
new("wood_crate", "wood", 0.9)
new("porcelain", "rock", 0.95)
new("brick", "rock", 0.47)
new("metal_box", "solidmetal", 0.5)
new("metalvent", "metal_box", 0.6, 0.45)
new("sand", "dirt", 0.3, 0.25)
new("rubber", "dirt", 0.85, 0.5)
new("gravel", "rock", 0.4)
new("glassbottle", "glass", 0.99)
new("pottery", "glassbottle", 0.95, 0.6)
new("tile", nil, 0.7, 0.3)
new("stone", "rock")
new("wood_solid", "wood", 0.8)
new("metalvehicle", "metal", 0.5)
new("cardboard", "dirt", 0.99, 0.95)
new("popcan", "metal_box")
new("canister", "metalpanel")
new("computer", "metal_box", 0.4, 0.45)
new("wood_plank", "wood_box", 0.85)
new("ceiling_tile", "cardboard")
new("hay", "cardboard")
new("wood_furniture", "wood_box")
new("paintcan", "popcan")
new("metal_barrel", "solidmetal", 0.01, 0.01)
new("metalgrate", nil, 0.95, 0.99)
new("mud", "dirt")
new("carpet", "dirt", 0.75)
new("wood_panel", "wood_crate")
new("snow", nil, 0.85)
new("concrete_block", "concrete")

local sv_penetration_type = CreateConVar("sv_penetration_type", "1", {FCVAR_REPLICATED, FCVAR_NOTIFY}, "What type of penertration to use. 0 = old CS, 1 = new penetration")
local MAX_PENETRATION_DISTANCE = 90 -- this is 7.5 feet

function SWEP:BulletCallback(atk, tr, dmg, vecDirBullet)
    local owner = self:GetPlayerOwner()
    if not owner then return end

    local nPenetrationCount = 4
    local iDamage = dmg:GetDamage()
    local vecDirShooting = vecDirBullet

    local fCurrentDamage = iDamage -- damage of the bullet at its current trajectory
    local flCurrentDistance = 0 -- distance that the bullet has traveled so far

    local _angShooting = vecDirShooting:Angle() _angShooting:Normalize()

    local flPenetration = self:GetPenetration()
    local flPenetrationPower = 0		-- thickness of a wall that this bullet can penetrate
    local flPenetrationDistance = 0		-- distance at which the bullet is capable of penetrating a wall
    local flDamageModifier = 0.5		-- default modification of bullets power after they go through a wall.
    local flPenetrationModifier = 1.0

    local params = SWITCH_BulletTypeParameters[self.ItemVisuals.primary_ammo]
    if sv_penetration_type:GetInt() == 1 then
        flPenetrationPower = 35
        flPenetrationDistance = 3000
    elseif params then
        flPenetrationPower = params.fPenetrationPower
        flPenetrationDistance = params.flPenetrationDistance
    end

    -- we use the max penetrations on this gun to figure out how much penetration it's capable of
    if sv_penetration_type:GetInt() == 1 then
        flPenetrationPower = self:GetPenetration()
    end

    local bFirstHit = true
    local lastPlayerHit = NULL -- includes players, bots, and npcs
    local bBulletHitPlayer = false

    local flDist_aim = 0
    local vHitLocation = Vector(0,0,0)

    local flDistance = self:GetRange()
    local vecSrc = owner:GetShootPos()

    local arrPendingDamage = {}
    local arrAlreadyHit = {[tr.Entity] = true}
    while fCurrentDamage > 0 do
        local vecEnd = vecSrc + vecDirShooting * (flDistance - flCurrentDistance)
        local tr = {} -- main enter bullet trace

        util.TraceLine({
            start = vecSrc,
            endpos = vecEnd,
            mask = bit.bor(CS_MASK_SHOOT, CONTENTS_HITBOX),
            filter = {owner, lastPlayerHit},
            collisiongroup = COLLISION_GROUP_NONE,
            output = tr
        })

        if flDist_aim == 0 then
            flDist_aim = (tr.Fraction ~= 1) and (tr.StartPos - tr.HitPos):Length() or 0
        end

        if flDist_aim ~= 0 then
             vHitLocation = tr.HitPos
        end

        lastPlayerHit = tr.Entity

        if lastPlayerHit:IsValid() then
            -- more stuff
            --[[
                if ( lastPlayerHit->GetTeamNumber() == GetTeamNumber() )
                {
                    bShotHitTeammate = true;
                }

                bBulletHitPlayer = true;
            ]]
        end

        -- we didn't hit anything, stop tracing shoot
        if tr.Fraction == 1 then break end

        --[[
            client accuracy debug here
        ]]

        -- /************* MATERIAL DETECTION ***********/
        local iEnterMaterial
        if tr.SurfaceProps == -1 then
            if SWCS_DEBUG_PENETRATION:GetBool() then
                print(Format("enter material not engine registered at (%f %f %f), using default instead",
                    tr.HitPos.x, tr.HitPos.y, tr.HitPos.z))
            end
            tr.SurfaceProps = 0
        end

        local enterData = util.GetSurfaceData(tr.SurfaceProps)

        if enterData then
            iEnterMaterial = enterData.material
        end

        local pen_mat = pen_types[string.lower(util.GetSurfacePropName(tr.SurfaceProps))]
        if not pen_mat then
            if SWCS_DEBUG_PENETRATION:GetBool() then
                print(Format("no penetration material for %s, using default instead", util.GetSurfacePropName(tr.SurfaceProps)))
            end
            pen_mat = DEFAULT_PENETRATE
        end

        flPenetrationModifier = pen_mat.penetrationmodifier
        flDamageModifier = pen_mat.damagemodifier

        local bHitGrate = bit.band(tr.Contents, CONTENTS_GRATE) ~= 0

        if CLIENT then
            if sv_showimpacts:GetInt() == 1 or sv_showimpacts:GetInt() == 2 then
                -- draw red client impact markers
                debugoverlay.Box(tr.HitPos, -Vector(2,2,2), Vector(2,2,2), sv_showimpacts_time:GetFloat(), Color(255,0,0,127))
            end
        else
            if sv_showimpacts:GetInt() == 1 or sv_showimpacts:GetInt() == 3 then
                -- draw blue server impact markers
                debugoverlay.Box(tr.HitPos, -Vector(2,2,2), Vector(2,2,2), sv_showimpacts_time:GetFloat(), Color(0,0,255,127))
            end
        end

        flCurrentDistance = flCurrentDistance + (tr.Fraction * (flDistance - flCurrentDistance))
        fCurrentDamage = fCurrentDamage * math.pow(self:GetRangeModifier(), flCurrentDistance / 500)

        if bFirstHit then
            dmg:SetDamage(fCurrentDamage)
        end

        -- check if we reach penetration distance, no more penetrations after that
        -- or if our modifyer is super low, just stop the bullet
        if (flCurrentDistance > flPenetrationDistance and flPenetration > 0) or
            flPenetrationModifier < 0.1 then
            -- Setting nPenetrationCount to zero prevents the bullet from penetrating object at max distance
            -- and will no longer trace beyond the exit point, however "numPenetrationsInitiallyAllowedForThisBullet"
            -- is saved off to allow correct determination whether the hit on the object at max distance had
            -- *previously* penetrated anything or not. In case of a direct hit over 3000 units the saved off
            -- value would be max penetrations value and will determine a direct hit and not a penetration hit.
            -- However it is important that all tracing further stops past this point (as the code does at
            -- the time of writing) because otherwise next trace will think that 4 penetrations have already
            -- occurred.
            nPenetrationCount = 0;
        end

        local iDamageType = bit.bor(DMG_BULLET, DMG_NEVERGIB)
        -- if wep is taser, DMG_SHOCK | DMG_NEVERGIB
        dmg:SetDamageType(iDamageType)

        if not bFirstHit then
            util.BulletImpact(tr, owner)
        end

        bFirstHit = false

        if SERVER then
            local ent = tr.Entity

            table.insert(arrPendingDamage, {
                ent = ent,
                dmg = fCurrentDamage,
                dmgtype = iDamageType,
                trace = tr,
            })
        end

        if sv_showimpacts_penetration:GetInt() > 0 then
            local text = "^"
            local text2 = Format("%s%d", sv_showimpacts_penetration:GetInt() == 2 and "" or "DAMAGE APPLIED:  ", math.ceil(fCurrentDamage))
            local text3
            -- convert to meters
            --(100%% of shots will fall within a 30cm circle.)
            local flDistMeters = flCurrentDistance * 0.0254
            if flDistMeters >= 1 then
                text3 = Format("%s%0.1fm", sv_showimpacts_penetration:GetInt() == 2 and "" or "TOTAL DISTANCE:  ", flDistMeters)
            else
                text3 = Format("%s%0.1fcm", sv_showimpacts_penetration:GetInt() == 2 and "" or "TOTAL DISTANCE:  ", flDistMeters / 0.01)
            end

            local textPos = tr.HitPos

            debugoverlay.EntityTextAtPosition(textPos, 1, text, sv_showimpacts_time:GetFloat(), Color(255,128,64,255))
            debugoverlay.EntityTextAtPosition(textPos, 2, text2, sv_showimpacts_time:GetFloat(), Color(255,64,0,255))
            debugoverlay.EntityTextAtPosition(textPos, 3, text3, sv_showimpacts_time:GetFloat(), Color(255,128,0,255))

            debugoverlay.Box(tr.HitPos, -Vector(0.8,0.8,0.8), Vector(0.8,0.8,0.8), sv_showimpacts_time:GetFloat(), Color(255,100,50,64))
        end

        local bulletStopped
        bulletStopped, nPenetrationCount, flPenetration, fCurrentDamage = self:HandleBulletPenetration( flPenetration, iEnterMaterial, hitGrate, tr, vecDirShooting, flPenetrationModifier,
            flDamageModifier, iDamageType, flPenetrationPower, nPenetrationCount, vecSrc, flDistance,
            flCurrentDistance, fCurrentDamage )

        if bulletStopped then break end
    end

    for i, t in ipairs(arrPendingDamage) do
        local ent = t.ent
        if arrAlreadyHit[ent] then continue end

        local dmg2 = DamageInfo()
        dmg2:SetAttacker(owner)
        dmg2:SetInflictor(self)

        dmg2:SetDamage(t.dmg)
        dmg2:SetDamageType(t.dmgtype)
        dmg2:SetDamageForce(t.trace.Normal)
        dmg2:SetDamagePosition(t.trace.StartPos)

        hook.Run("ScalePlayerDamage", ent, t.trace.HitGroup, dmg2)
        ent:TakeDamageInfo(dmg2)
        arrAlreadyHit[ent] = true
    end

    return false
end

local function IsBreakableEntity(ent)
    if not IsValid(ent) then return false end

    -- first check to see if it's already broken
    if ent:Health() < 0 and ent:GetMaxHealth() > 0 then
        return true
    end

    -- If we won't be able to break it, don't try
    if SERVER then
        local var = ent:GetInternalVariable("m_takedamage")
        if tonumber(var) and tonumber(var) ~= 2 then
            return false
        end
    end

    if ent:GetCollisionGroup() ~= COLLISION_GROUP_PUSHAWAY and ent:GetCollisionGroup() ~= COLLISION_GROUP_BREAKABLE_GLASS and ent:GetCollisionGroup() ~= COLLISION_GROUP_NONE then
        return false
    end

    if ent:Health() > 200 then
        return false
    end

    if ent:GetClass() == "func_breakable" or ent:GetClass() == "func_breakable_surf" then
        return true
    end

    return true
end

function SWEP:TraceToExit(start, dir, endpos, trEnter, trExit, flStepSize, flMaxDistance)
    local flDistance = 0
    local nStartContents = 0

    while flDistance <= flMaxDistance do
        flDistance = flDistance + flStepSize

        endpos:Set(start + (flDistance * dir))

        local vecTrEnd = endpos - (flStepSize * dir)

        if nStartContents == 0 then
            nStartContents = bit.band(util.PointContents(endpos), bit.bor(CS_MASK_SHOOT, CONTENTS_HITBOX))
        end

        local nCurrentContents = bit.band(util.PointContents(endpos), bit.bor(CS_MASK_SHOOT, CONTENTS_HITBOX))

        if bit.band(nCurrentContents, CS_MASK_SHOOT) == 0 or ((bit.band(nCurrentContents, CONTENTS_HITBOX) ~= 0) and nStartContents ~= nCurrentContents) then
            -- this gets a bit more complicated and expensive when we have to deal with displacements
            util.TraceLine({
                start = endpos,
                endpos = vecTrEnd,
                mask = bit.bor(CS_MASK_SHOOT, CONTENTS_HITBOX),
                output = trExit,
            })

            -- we exited the wall into a player's hitbox
            if trExit.StartSolid and (bit.band(trExit.SurfaceFlags, SURF_HITBOX) ~= 0) then
                -- do another trace, but skip the player to get the actual exit surface
                util.TraceLine({
                    start = endpos,
                    endpos = start,
                    mask = bit.bor(CS_MASK_SHOOT, CONTENTS_HITBOX),
                    filter = trExit.Entity,
                    collisiongroup = COLLISION_GROUP_NONE,
                    output = trExit
                })

                if trExit.Hit and not trExit.StartSolid then
                    endpos:Set(trExit.HitPos)
                    return true
                end
            elseif trExit.Hit and not trExit.StartSolid then
                local bStartIsNodraw = bit.band(trEnter.SurfaceFlags, SURF_NODRAW) ~= 0
                local bExitIsNodraw = bit.band(trExit.SurfaceFlags, SURF_NODRAW) ~= 0

                if bExitIsNodraw and IsBreakableEntity(trExit.Entity) and IsBreakableEntity(trEnter.Entity) then
                    -- we have a case where we have a breakable object, but the mapper put a nodraw on the backside
                    endpos:Set(trExit.HitPos)
                    return true
                elseif not bExitIsNodraw or (bStartIsNodraw and bExitIsNodraw) then -- exit nodraw is only valid if our entrace is also nodraw
                    local vecNormal = trExit.HitNormal
                    local flDot = dir:Dot(vecNormal)
                    if flDot <= 1 then
                        -- get the real end pos
                        endpos:Set(endpos - ((flStepSize * trExit.Fraction) * dir))
                        return true
                    end
                end
            elseif (trEnter.Entity ~= game.GetWorld()) and IsBreakableEntity(trEnter.Entity) then
                -- if we hit a breakable, make the assumption that we broke it if we can't find an exit (hopefully..)
                -- fake the end pos
                table.CopyFromTo(trEnter, trExit)
                trExit.HitPos = start + (1 * dir)
                return true
            end
        end
    end

    return false
end

local CHAR_TEX_CARDBOARD = -1
function SWEP:HandleBulletPenetration(flPenetration,
                iEnterMaterial,
                bHitGrate,
                tr,
                vecDir,
                flPenetrationModifier,
                flDamageModifier,
                iDamageType,
                flPenetrationPower,
                nPenetrationCount,
                vecSrc,
                flDistance,
                flCurrentDistance,
                fCurrentDamage)

    local bIsNodraw = bit.band(tr.SurfaceFlags, SURF_NODRAW) ~= 0
    local bFailedPenetrate = false

    -- check if bullet can penetrarte another entity
    if nPenetrationCount == 0 and not bHitGrate and not bIsNodraw
        and iEnterMaterial ~= MAT_GLASS and iEnterMaterial ~= MAT_GRATE then
        bFailedPenetrate = true -- no, stop
    end

    -- If we hit a grate with iPenetration == 0, stop on the next thing we hit
    if flPenetration <= 0 or nPenetrationCount <= 0 then
        bFailedPenetrate = true
    end

    local penetrationEnd = Vector()

    -- find exact penetration exit
    local exitTr = {}
    if not self:TraceToExit(tr.HitPos, vecDir, penetrationEnd, tr, exitTr, 4, MAX_PENETRATION_DISTANCE) then
        -- ended in solid
        if bit.band(util.PointContents(tr.HitPos), CS_MASK_SHOOT) == 0 then
            bFailedPenetrate = true
        end
    end

    if bFailedPenetrate then
        local flTraceDistance = (penetrationEnd - tr.HitPos):Length()

        -- this is copy pasted from below, it should probably be its own function
        local flPenMod = math.max(0, 1 / flPenetrationModifier)
        local flPercentDamageChunk = fCurrentDamage * 0.15
        local flDamageLostImpact = flPercentDamageChunk + math.max(0, ( 3 / flPenetrationPower) * 1.18) * (flPenMod * 2.8)

        local flLostDamageObject = ( ( flPenMod * (flTraceDistance * flTraceDistance) ) / 24)
        local flTotalLostDamage = flDamageLostImpact + flLostDamageObject

        self:DisplayPenetrationDebug( tr.HitPos, penetrationEnd, flTraceDistance, fCurrentDamage, flDamageLostImpact, flTotalLostDamage, tr.SurfaceProps, -100 )
        return true, nPenetrationCount, flPenetration, fCurrentDamage
    end

    local iExitMaterial
    if table.IsEmpty(exitTr) then
        if SWCS_DEBUG_PENETRATION:GetBool() then
            print("exit trace empty???, stopping penetration!!!!")
        end

        return true, nPenetrationCount, flPenetration, fCurrentDamage
    end
    local exitSurfaceData = util.GetSurfaceData(exitTr.SurfaceProps)

    if exitSurfaceData then
        iExitMaterial = exitSurfaceData.material
    end
    if not iExitMaterial then
        if SWCS_DEBUG_PENETRATION:GetBool() then
            print(Format("exit material not engine registered at (%f %f %f), using default instead",
                tr.HitPos.x, tr.HitPos.y, tr.HitPos.z))
        end
        iExitMaterial = util.GetSurfaceData(SURFACE_PROP_DEFAULT).material
    end

    -- new penetration method
    if sv_penetration_type:GetInt() == 1 then
        -- percent of total damage lost automatically on impacting a surface
        local flDamLostPercent = 0.16

        -- since some railings in de_inferno are CONTENTS_GRATE but CHAR_TEX_CONCRETE, we'll trust the
        -- CONTENTS_GRATE and use a high damage modifier.
        if bHitGrate or bIsNodraw or iEnterMaterial == MAT_GLASS or iEnterMaterial == MAT_GRATE then
            -- If we're a concrete grate (TOOLS/TOOLSINVISIBLE texture) allow more penetrating power.
            if iEnterMaterial == MAT_GRATE or iEnterMaterial == MAT_GRATE then
                flPenetrationModifier = 3
                flDamLostPercent = 0.05
            else
                flPenetrationModifier = 1
            end

            flDamageModifier = 0.99
        else
            -- check the exit material and average the exit and entrace values
            local pen_mat
            if exitSurfaceData then
                pen_mat = pen_types[string.lower(exitSurfaceData.name)]
            end

            if not pen_mat then
                pen_mat = DEFAULT_PENETRATE
            end

            local flExitPenetrationModifier = pen_mat.penetrationmodifier
            local flExitDamageModifier = pen_mat.damagemodifier
            flPenetrationModifier = (flPenetrationModifier + flExitPenetrationModifier) / 2
            flDamageModifier = (flDamageModifier + flExitDamageModifier) / 2
        end

        -- if enter & exit point is wood we assume this is 
        -- a hollow crate and give a penetration bonus
        if iEnterMaterial == iExitMaterial then
            if iExitMaterial == MAT_WOOD or iExitMaterial == CHAR_TEX_CARDBOARD then
                flPenetrationModifier = 3
            elseif iExitMaterial == MAT_PLASTIC then
                flPenetrationModifier = 2
            end
        end

        local flTraceDistance = (exitTr.HitPos - tr.HitPos):Length()
        local flPenMod = math.max(0, 1 / flPenetrationModifier)

        local flPercentDamageChunk = fCurrentDamage * flDamLostPercent
        local flPenWepMod = flPercentDamageChunk + math.max(0, (3 / flPenetrationPower) * 1.25) * (flPenMod * 3)

        local flLostDamageObject = ((flPenMod * (flTraceDistance * flTraceDistance)) / 24)
        local flTotalLostDamage = flPenWepMod + flLostDamageObject

        if sv_showimpacts_penetration:GetInt() > 0 then
            local vecStart = tr.HitPos
            local vecEnd = penetrationEnd
            local flTotalTraceDistance = (penetrationEnd - tr.HitPos):Length()

            self:DisplayPenetrationDebug(vecStart, vecEnd, flTotalTraceDistance, fCurrentDamage, flPenWepMod, flTotalLostDamage, tr.SurfaceProps, exitTr.SurfaceProps ) -- extra shit here pls dont forget novus
        end

        -- reduce damage each time we hit something other than a grate
        fCurrentDamage = fCurrentDamage - math.max(0, flTotalLostDamage)
        if fCurrentDamage < 1 then
            return true, nPenetrationCount, flPenetration, fCurrentDamage
        end

        -- penetration was successful

        -- bullet did penetrate object, exit Decal
        util.BulletImpact(exitTr, owner)

        -- setup new start end parameters for successive trace
        flCurrentDistance = flCurrentDistance + flTraceDistance
        vecSrc:Set(exitTr.HitPos)
        flDistance = (flDistance - flCurrentDistance) * 0.5

        nPenetrationCount = nPenetrationCount - 1
        return false, nPenetrationCount, flPenetration, fCurrentDamage
    else
        -- old, gay method
        --[[
            // get material at exit point
            surfacedata_t *pExitSurfaceData = physprops->GetSurfaceData( exitTr.surface.surfaceProps );
            int iExitMaterial = pExitSurfaceData->game.material;

            // old penetration method
            if ( sv_penetration_type.GetInt() != 1 )
            {
                // since some railings in de_inferno are CONTENTS_GRATE but CHAR_TEX_CONCRETE, we'll trust the
                // CONTENTS_GRATE and use a high damage modifier.
                if ( hitGrate || bIsNodraw )
                {
                    // If we're a concrete grate (TOOLS/TOOLSINVISIBLE texture) allow more penetrating power.
                    flPenetrationModifier = 1.0f;
                    flDamageModifier = 0.99f;
                }
                else
                {
                    // Check the exit material to see if it is has less penetration than the entrance material.
                    float flExitPenetrationModifier = pExitSurfaceData->game.penetrationModifier;
                    float flExitDamageModifier = pExitSurfaceData->game.damageModifier;
                    if ( flExitPenetrationModifier < flPenetrationModifier )
                    {
                        flPenetrationModifier = flExitPenetrationModifier;
                    }
                    if ( flExitDamageModifier < flDamageModifier )
                    {
                        flDamageModifier = flExitDamageModifier;
                    }
                }

                // if enter & exit point is wood or metal we assume this is 
                // a hollow crate or barrel and give a penetration bonus
                if ( iEnterMaterial == iExitMaterial )
                {
                    if ( iExitMaterial == CHAR_TEX_WOOD ||
                        iExitMaterial == CHAR_TEX_METAL )
                    {
                        flPenetrationModifier *= 2;
                    }
                }

                float flTraceDistance = VectorLength( exitTr.endpos - tr.endpos );

                // check if bullet has enough power to penetrate this distance for this material
                if ( flTraceDistance > ( flPenetrationPower * flPenetrationModifier ) )
                    return true; // bullet hasn't enough power to penetrate this distance

                // reduce damage power each time we hit something other than a grate
                fCurrentDamage *= flDamageModifier;

                // penetration was successful

                // bullet did penetrate object, exit Decal
                if ( bDoEffects )
                {
                    UTIL_ImpactTrace( &exitTr, iDamageType );
                }

                #ifndef CLIENT_DLL
                    // decal players on the server to eliminate the disparity between where the client thinks the decal went and where it actually went
                    // we want to eliminate the case where a player sees a blood decal on someone, but they are at 100 health
                    if ( sv_server_verify_blood_on_player.GetBool() && tr.DidHit() && tr.m_pEnt && tr.m_pEnt->IsPlayer() )
                    {
                        UTIL_ImpactTrace( &tr, iDamageType );
                    }
                #endif

                //setup new start end parameters for successive trace

                flPenetrationPower -= flTraceDistance / flPenetrationModifier;
                flCurrentDistance += flTraceDistance;

                // NDebugOverlay::Box( exitTr.endpos, Vector(-2,-2,-2), Vector(2,2,2), 0,255,0,127, 8 );

                vecSrc = exitTr.endpos;
                flDistance = ( flDistance - flCurrentDistance ) * 0.5;

                // reduce penetration counter
                nPenetrationCount--;
                return false;
            }
        ]]
    end
    return true, nPenetrationCount, flPenetration, fCurrentDamage
end

function SWEP:DisplayPenetrationDebug(vecEnter, vecExit, flDistance, flInitialDamage, flDamageLostImpact, flTotalLostDamage, nEnterSurf, nExitSurf)
    if SERVER and sv_showimpacts_penetration:GetInt() > 0 then
        local vecStart = vecEnter
        local vecEnd = vecExit
        local flTotalTraceDistance = (vecExit - vecEnd):Length()

        if flTotalLostDamage >= flInitialDamage then
            nExitSurf = -100

            local flLostDamageObject = (flTotalLostDamage - flDamageLostImpact)
            local flFrac = math.max(0, (flInitialDamage - flDamageLostImpact) / flLostDamageObject)
            vecEnd = vecEnd - vecStart
            vecEnd:Normalize()
            vecEnd = vecStart + (vecEnd * flTotalTraceDistance * flFrac)

            if flDamageLostImpact >= flInitialDamage then
                flDistance = 0
                vecStart = vecEnd
            end

            flTotalLostDamage = math.ceil(flInitialDamage)
        end

        local textPos = vecEnd * 1
        local text = ""

        if flTotalLostDamage < flInitialDamage then
            local flDistMeters = flDistance * 0.0254
            if flDistMeters >= 1 then
                text = Format("%s%0.1fm", sv_showimpacts_penetration:GetInt() == 2 and "" or "THICKNESS:		", flDistMeters)
            else
                text = Format("%s%0.1fcm", sv_showimpacts_penetration:GetInt() == 2 and "" or "THICKNESS:		", flDistMeters / 0.01)
            end
        else
            text = "STOPPED!"
        end

        debugoverlay.EntityTextAtPosition(textPos, -3, text, sv_showimpacts_time:GetFloat(), Color(220, 128, 128, 255))

        local text3 = Format("%s%0.1f", sv_showimpacts_penetration:GetInt() == 2 and "-" or "LOST DAMAGE:		", flTotalLostDamage)
        debugoverlay.EntityTextAtPosition(textPos, -2, text3, sv_showimpacts_time:GetFloat(), Color(90, 22, 0, 160))

        local textmat1 = Format("%s", nEnterSurf and util.GetSurfacePropName(nEnterSurf) or "nil")
        debugoverlay.EntityTextAtPosition(vecStart, -1, textmat1, sv_showimpacts_time:GetFloat(), Color(0,255,0,128))

        if nExitSurf ~= -100 then
            debugoverlay.Box(vecStart, -Vector(0.4,0.4,0.4), Vector(0.4,0.4,0.4), sv_showimpacts_time:GetFloat(), Color(0,255,0,128))

            local textmat2 = Format("%s", nExitSurf and nExitSurf == -1 and "" or (nExitSurf and util.GetSurfacePropName( nExitSurf ) or "nil") )
            debugoverlay.Box(vecEnd, -Vector(0.4,0.4,0.4), Vector(0.4,0.4,0.4), sv_showimpacts_time:GetFloat(), Color(0,128,255,128))
            debugoverlay.EntityTextAtPosition(vecEnd, -1, textmat2, sv_showimpacts_time:GetFloat(), Color(0,128,255,128))

            if flDistance > 0 and vecStart ~= vecEnd then
                debugoverlay.Line(vecStart, vecEnd, sv_showimpacts_time:GetFloat(), Color(0,190,190), true)
            end
        else
            -- different color
            debugoverlay.Box(vecStart, -Vector(0.4,0.4,0.4), Vector(0.4,0.4,0.4), sv_showimpacts_time:GetFloat(), Color(160,255,0,128))
            debugoverlay.Line(vecStart, vecEnd, sv_showimpacts_time:GetFloat(), Color(190,190,0), true)
        end
    end
end

local function ApplyIronSightScopeEffect(wep, x, y, w, h, bPreparationStage)
    local ply = wep:GetPlayerOwner()
    if not ply then return end

    local iron = wep.m_IronSightController
    if not iron then return end

    if bPreparationStage then
        return iron:PrepareScopeEffect(x, y, w, h)
    else
        iron:RenderScopeEffect(x, y, w, h)
    end
end

function SWEP:ViewModelDrawn(vm)
    if self:GetInReload() then
        --self:CamDriverCollect(vm)
    end

    -- ApplyIronSightScopeEffect( view.x, view.y, view.width, view.height, &m_CurrentView, true )
    -- draw vms again, put into stencil buffer?
    -- ApplyIronSightScopeEffect( view.x, view.y, view.width, view.height, &m_CurrentView, false ) ; which will render the dot and blur into the screen
end
